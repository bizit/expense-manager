-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.39-community


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema expense_manager
--

CREATE DATABASE IF NOT EXISTS expense_manager;
USE expense_manager;

--
-- Definition of table `branch`
--

DROP TABLE IF EXISTS `branch`;
CREATE TABLE `branch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch`
--

/*!40000 ALTER TABLE `branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `branch` ENABLE KEYS */;


--
-- Definition of table `expense`
--

DROP TABLE IF EXISTS `expense`;
CREATE TABLE `expense` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `expense_no` varchar(255) NOT NULL,
  `expense_sub_type` int(10) NOT NULL,
  `expense_amount` double NOT NULL,
  `expense_date` datetime NOT NULL,
  `recurring_expense` int(10) DEFAULT NULL,
  `expense_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `expense_no` (`expense_no`),
  KEY `FKExpense164278` (`expense_sub_type`),
  KEY `FKExpense926169` (`recurring_expense`),
  CONSTRAINT `FKExpense164278` FOREIGN KEY (`expense_sub_type`) REFERENCES `expense_sub_type` (`id`),
  CONSTRAINT `FKExpense926169` FOREIGN KEY (`recurring_expense`) REFERENCES `recurring_expense` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense`
--

/*!40000 ALTER TABLE `expense` DISABLE KEYS */;
/*!40000 ALTER TABLE `expense` ENABLE KEYS */;


--
-- Definition of table `expense_main_type`
--

DROP TABLE IF EXISTS `expense_main_type`;
CREATE TABLE `expense_main_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense_main_type`
--

/*!40000 ALTER TABLE `expense_main_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `expense_main_type` ENABLE KEYS */;


--
-- Definition of table `expense_payment`
--

DROP TABLE IF EXISTS `expense_payment`;
CREATE TABLE `expense_payment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `payment_date` datetime NOT NULL,
  `amount` double NOT NULL,
  `description` text,
  `Expenseid` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKExpense_Pa663084` (`Expenseid`),
  CONSTRAINT `FKExpense_Pa663084` FOREIGN KEY (`Expenseid`) REFERENCES `expense` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense_payment`
--

/*!40000 ALTER TABLE `expense_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `expense_payment` ENABLE KEYS */;


--
-- Definition of table `expense_refund`
--

DROP TABLE IF EXISTS `expense_refund`;
CREATE TABLE `expense_refund` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `expense_payment` int(10) NOT NULL,
  `refund_date` datetime NOT NULL,
  `amount` double NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `FKExpense_Re14270` (`expense_payment`),
  CONSTRAINT `FKExpense_Re14270` FOREIGN KEY (`expense_payment`) REFERENCES `expense_payment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense_refund`
--

/*!40000 ALTER TABLE `expense_refund` DISABLE KEYS */;
/*!40000 ALTER TABLE `expense_refund` ENABLE KEYS */;


--
-- Definition of table `expense_sub_type`
--

DROP TABLE IF EXISTS `expense_sub_type`;
CREATE TABLE `expense_sub_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `expense_main_type` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `FKExpense_Su461895` (`expense_main_type`),
  CONSTRAINT `FKExpense_Su461895` FOREIGN KEY (`expense_main_type`) REFERENCES `expense_main_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense_sub_type`
--

/*!40000 ALTER TABLE `expense_sub_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `expense_sub_type` ENABLE KEYS */;


--
-- Definition of table `permission`
--

DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `role` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKPermission910751` (`role`),
  CONSTRAINT `FKPermission910751` FOREIGN KEY (`role`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission`
--

/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;


--
-- Definition of table `recurring_expense`
--

DROP TABLE IF EXISTS `recurring_expense`;
CREATE TABLE `recurring_expense` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `recurring_duration` int(2) NOT NULL,
  `recurring_type` varchar(255) NOT NULL,
  `expense_sub_type` int(10) NOT NULL,
  `expense_amount` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKRecurring_594952` (`expense_sub_type`),
  CONSTRAINT `FKRecurring_594952` FOREIGN KEY (`expense_sub_type`) REFERENCES `expense_sub_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recurring_expense`
--

/*!40000 ALTER TABLE `recurring_expense` DISABLE KEYS */;
/*!40000 ALTER TABLE `recurring_expense` ENABLE KEYS */;


--
-- Definition of table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


--
-- Definition of table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `role` int(10) NOT NULL,
  `branch` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`),
  KEY `FKUser708634` (`role`),
  KEY `FKUser423445` (`branch`),
  CONSTRAINT `FKUser423445` FOREIGN KEY (`branch`) REFERENCES `branch` (`id`),
  CONSTRAINT `FKUser708634` FOREIGN KEY (`role`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


--
-- Definition of table `user_permission`
--

DROP TABLE IF EXISTS `user_permission`;
CREATE TABLE `user_permission` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user` int(10) NOT NULL,
  `permission` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKUser_Permi223152` (`user`),
  KEY `FKUser_Permi764374` (`permission`),
  CONSTRAINT `FKUser_Permi764374` FOREIGN KEY (`permission`) REFERENCES `permission` (`id`),
  CONSTRAINT `FKUser_Permi223152` FOREIGN KEY (`user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_permission`
--

/*!40000 ALTER TABLE `user_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_permission` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
