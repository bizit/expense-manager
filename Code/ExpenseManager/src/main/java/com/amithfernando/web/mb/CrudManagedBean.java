/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.web.mb;

import org.primefaces.event.SelectEvent;

/**
 *
 * @author Amith Fernando
 */
public interface CrudManagedBean {
    
   public static String SAVE_MSG="Save Succeed!";
    public static String UPDATE_MSG="Update Succeed!";
    public static String DELETE_MSG="Delete Succeed!";
    public static String PLEASE_SELECT_RECORD="Please select record";
    public static String NO_PERMISSION="You haven't permision";
    public static String DUPLICATE_RECORD="Duplicate Record!";
     public static String PLEASE_DELETE_CHILD_DATA="Please delete child data first!";
    
    public void newAction();
    public void saveAction();
    public boolean isValidOnSave();
    public boolean isVaildOnEdit();
    public void deleteAction();
    public void onDeleteBtnClick();
    public boolean isCanDelete();
    public void searchAction();
     public void onRecordSelect(SelectEvent evt);
    
}
