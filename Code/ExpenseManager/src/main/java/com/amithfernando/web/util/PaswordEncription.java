/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.web.util;

import java.util.Random;
import org.apache.shiro.crypto.hash.Sha256Hash;

/**
 *
 * @author Amith Ishanka
 */
public class PaswordEncription {


    public static String encript(String password){
        Sha256Hash sha256Hash = new Sha256Hash(password);
        return sha256Hash.toHex();
    }
    
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static Random rnd = new Random();

    public static String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }
}
