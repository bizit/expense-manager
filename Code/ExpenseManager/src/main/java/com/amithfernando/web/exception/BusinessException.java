/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.web.exception;

import javax.ejb.ApplicationException;

/**
 *
 * @author Amith Fernando
 */
@ApplicationException(rollback = true)
public abstract class BusinessException extends RuntimeException {

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }
    
    
    
}
