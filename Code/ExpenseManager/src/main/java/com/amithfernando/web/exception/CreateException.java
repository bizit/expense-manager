/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.web.exception;

/**
 *
 * @author Amith Fernando
 */
public class CreateException extends BusinessException{

    public CreateException(String message, Throwable cause) {
        super(message, cause);
    }
 
    
}
