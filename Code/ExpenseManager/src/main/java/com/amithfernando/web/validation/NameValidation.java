/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.web.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Amith
 */
@FacesValidator("com.amithfernando.web.validation.NameValidator")
public class NameValidation implements Validator {

    private static final String PATTERN = "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$";
    private Pattern pattern;
    private Matcher matcher;

    public NameValidation() {
        pattern = Pattern.compile(PATTERN);
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        String v=value.toString();
        if (!v.equals("")) {
            matcher = pattern.matcher(value.toString());
            if (!matcher.matches()) {

                FacesMessage msg =
                        new FacesMessage("String validation failed.",
                        "Invalid String format.");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);

            }
        }

    }
}

