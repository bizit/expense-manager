/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.web.util;

import java.util.HashMap;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author Amith Ishanka
 */
public class JasperReportUtil {
    
    
    public static void toPDF(JRBeanCollectionDataSource beanCollectionDataSource,String reportPath,Map<String,Object>params,String reportName) throws Exception{              
        JasperPrint jasperPrint = JasperFillManager.fillReport(reportPath, params, beanCollectionDataSource);
        HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "inline; filename="+reportName+".pdf");
        ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
        FacesContext.getCurrentInstance().responseComplete();
    }
}
