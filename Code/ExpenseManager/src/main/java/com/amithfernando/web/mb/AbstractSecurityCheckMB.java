/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.web.mb;

import com.amithfernando.web.util.JsfUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 *
 * @author Amith Fernando
 */
public class AbstractSecurityCheckMB {

    protected Subject currentUser;

    protected void initSecurityCheck() {
        currentUser = SecurityUtils.getSubject();
    }

    public String getLoggedUserName() {
        return JsfUtil.getSessionAttrtibute("user").toString();
    }
}
