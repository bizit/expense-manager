/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.web.util;

import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.inject.Named;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Amith Ishanka
 */
@Stateless
public class EmailBean {

    @Resource(name = "mail/SystemMail")
    private Session mailSession;

    @Asynchronous
    public void sendMessage(String email, String subject, String bodyMessage) {
        Message message = new MimeMessage(mailSession);
        try {
            message.setFrom(new InternetAddress(mailSession.getProperty("mail.from")));
            //
            System.out.println("Email Sending " + email + " subject " + subject);
            message.setSubject(subject);
            message.setRecipient(RecipientType.TO, new InternetAddress(email));
            // this is if you want message body as text
            //message.setText(bodyMessage); 
            // use this is if you want to send html based message
            message.setContent(bodyMessage, "text/html; charset=utf-8");
            // This is not mandatory, however, it is a good
            // practice to indicate the software which
            // constructed the message.
            message.setHeader("X-Mailer", "MINIpay mailer www.minipay.eu");
            // Adjust the date of sending the message
            Date timeStamp = new Date();
            message.setSentDate(timeStamp);
            Transport.send(message);
            System.out.println("Send Success " + email + " subject " + subject);
        } catch (AddressException ex) {
            System.out.println("### -> No Email Address");
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }
    }
}
