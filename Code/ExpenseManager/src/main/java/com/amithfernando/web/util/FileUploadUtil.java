/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.web.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Amith
 */
public class FileUploadUtil {

    public static final String TEMP_PATH = "resources/uploads/temp/";
    public static final String DOCUMENT_PATH = "resources/uploads/document/";
    public static final String EMPLOYEE_PATH = "resources/uploads/employee/";
    public static final String EMP_DOCUMENT_PATH = "resources/uploads/emp_document/";

    public static String getRealPath() {
        String path = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
        return path;
    }

    public static String fileUpoadTemporary(FileUploadEvent event, String uploadedPath) throws Exception {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmss");
        String name = fmt.format(new Date()) + event.getFile().getFileName().substring(event.getFile().getFileName().lastIndexOf('.'));
        File file = new File(getRealPath() + uploadedPath + name);

        InputStream is = event.getFile().getInputstream();
        OutputStream out = new FileOutputStream(file);
        byte buf[] = new byte[1024];
        int len;
        while ((len = is.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        is.close();
        out.close();
        return name;
    }

    public static void moveFile(String newPath, String newName, String oldName) {
        File oldFile = new File(getRealPath() + TEMP_PATH + oldName);
        File newFile = new File(getRealPath() + newPath + newName);
        oldFile.renameTo(newFile);
    }

    public static void deleteFile(String path, String fileName) {
        File result = null;
        result = new File(getRealPath() + path + fileName);
        if (result.exists()) {
            result.delete();
        }
    }

    public static boolean isFileExsist(String path, String fileName) {
        File result = null;
        result = new File(getRealPath() + path + fileName);
        if (result.exists()) {
            return true;
        } else {
            return false;
        }
    }

    public static StreamedContent getFileStreamedContent(String path, String fileName, String contentType) throws Exception {
        System.out.println(""+getRealPath() + path + fileName);
        InputStream is = new BufferedInputStream(new FileInputStream(getRealPath() + path + fileName));
        return new DefaultStreamedContent(is, contentType, fileName);
    }
}
