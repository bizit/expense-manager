/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.web.mb;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 *
 * @author Amith Fernando
 */
@ManagedBean
@RequestScoped
public class NavigationMB implements Serializable {

    private boolean isAdmin;
    private boolean isDataEntry;
    private boolean isCustomer;

    @PostConstruct
    private void init() {
        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser.hasRole("Admin")) {
            isAdmin = true;
        } else if (currentUser.hasRole("DataEntry")) {
            isDataEntry = true;
        } else if(currentUser.hasRole("Customer")){
            isCustomer = true;
        }
    }

    /*
     * GETTER & SETTER #########################################################
     */
    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public boolean isIsDataEntry() {
        return isDataEntry;
    }

    public void setIsDataEntry(boolean isDataEntry) {
        this.isDataEntry = isDataEntry;
    }

    public boolean isIsCustomer() {
        return isCustomer;
    }

    public void setIsCustomer(boolean isCustomer) {
        this.isCustomer = isCustomer;
    }
    
    
}
