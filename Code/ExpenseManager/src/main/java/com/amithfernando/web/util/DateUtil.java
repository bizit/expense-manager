/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.web.util;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Months;
import org.joda.time.Years;

/**
 *
 * @author Amith
 */
public class DateUtil {

    public static final Date stringToDate(String sDate, String format) throws ParseException {
        Date date = new SimpleDateFormat(format).parse(sDate);
        return date;
    }

    public static final String dateToString(Date date, String format) {
        Format formatter = new SimpleDateFormat(format);
        String sDate = formatter.format(date);
        return sDate;
    }

    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

    public static int dateDiffrence(Date source, Date target, String type) {
        int difference = 0;
        DateTime start = new DateTime(source);
        DateTime end = new DateTime(target);

        if (source.after(target)) {
            start = end;
            end = new DateTime(source);
        }

        if (type.equals("MONTH")) {
            difference = Months.monthsBetween(start, end).getMonths();
        } else if (type.equals("DAYS")) {
            difference = Days.daysBetween(start, end).getDays();
        } else if (type.equals("YEARS")) {
            difference = Years.yearsBetween(start, end).getYears();
        } else if (type.equals("HOURS")) {
            difference = Hours.hoursBetween(start, end).getHours();
        } else if (type.equals("MINITUES")) {
            difference = Minutes.minutesBetween(start, end).getMinutes();
        }

        return difference;
    }

}
