/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cbcit.expensemanager.domains;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Amith
 */
@Entity
@Table(name = "expense_payment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExpensePayment.findAll", query = "SELECT e FROM ExpensePayment e"),
    @NamedQuery(name = "ExpensePayment.findById", query = "SELECT e FROM ExpensePayment e WHERE e.id = :id"),
    @NamedQuery(name = "ExpensePayment.findByPaymentDate", query = "SELECT e FROM ExpensePayment e WHERE e.paymentDate = :paymentDate"),
    @NamedQuery(name = "ExpensePayment.findByAmount", query = "SELECT e FROM ExpensePayment e WHERE e.amount = :amount")
})
public class ExpensePayment implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "payment_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date paymentDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "expensePayment")
    private List<ExpenseRefund> expenseRefundList;
    @JoinColumn(name = "Expenseid", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Expense expenseid;

    public ExpensePayment() {
    }

    public ExpensePayment(Integer id) {
        this.id = id;
    }

    public ExpensePayment(Integer id, Date paymentDate, double amount) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.amount = amount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public List<ExpenseRefund> getExpenseRefundList() {
        return expenseRefundList;
    }

    public void setExpenseRefundList(List<ExpenseRefund> expenseRefundList) {
        this.expenseRefundList = expenseRefundList;
    }

    public Expense getExpenseid() {
        return expenseid;
    }

    public void setExpenseid(Expense expenseid) {
        this.expenseid = expenseid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExpensePayment)) {
            return false;
        }
        ExpensePayment other = (ExpensePayment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cbcit.expensemanager.domains.ExpensePayment[ id=" + id + " ]";
    }
    
}
