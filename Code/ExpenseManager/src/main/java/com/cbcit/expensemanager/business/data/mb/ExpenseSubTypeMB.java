/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.data.mb;

import com.amithfernando.web.mb.AbstractSecurityCheckMB;
import com.amithfernando.web.mb.CrudManagedBean;
import static com.amithfernando.web.mb.CrudManagedBean.DELETE_MSG;
import static com.amithfernando.web.mb.CrudManagedBean.DUPLICATE_RECORD;
import static com.amithfernando.web.mb.CrudManagedBean.NO_PERMISSION;
import static com.amithfernando.web.mb.CrudManagedBean.PLEASE_SELECT_RECORD;
import static com.amithfernando.web.mb.CrudManagedBean.SAVE_MSG;
import static com.amithfernando.web.mb.CrudManagedBean.UPDATE_MSG;
import com.amithfernando.web.util.JsfUtil;
import com.cbcit.expensemanager.business.data.ejb.ExpenseSubTypeEjb;
import com.cbcit.expensemanager.business.expense.ejb.ExpenseEjb;
import com.cbcit.expensemanager.business.expense.ejb.RecurringExpenseEjb;
import com.cbcit.expensemanager.data.model.ExpenseSubTypeDataModel;
import com.cbcit.expensemanager.domains.ExpenseMainType;
import com.cbcit.expensemanager.domains.ExpenseSubType;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Amith
 */
@ManagedBean
@ViewScoped
public class ExpenseSubTypeMB extends AbstractSecurityCheckMB implements CrudManagedBean, Serializable {

    private ExpenseSubType expenseSubType;
    private ExpenseMainType searchExpenseMainType;
    private List<ExpenseSubType> expenseSubTypeList;
    private ExpenseSubTypeDataModel expenseSubTypeDataModel;
    @EJB
    private ExpenseSubTypeEjb expenseSubTypeEjb;

    @EJB
    private ExpenseEjb expenseEjb;

    @EJB
    private RecurringExpenseEjb recurringExpenseEjb;
    //
    private String validateMsg;

    @PostConstruct
    public void init() {
        newAction();
        searchAction();
        //
        initSecurityCheck();
    }

    @Override
    public void newAction() {
        expenseSubType = new ExpenseSubType();
    }

    @Override
    public void saveAction() {
        try {
            if (expenseSubType.getId() == null) {
                if (isValidOnSave()) {
                    expenseSubTypeEjb.save(expenseSubType);
                    JsfUtil.addSuccessMessage(SAVE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                if (isVaildOnEdit()) {
                    expenseSubTypeEjb.edit(expenseSubType);
                    JsfUtil.addSuccessMessage(UPDATE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            }
            searchAction();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValidOnSave() {
        if (!currentUser.isPermitted("expenseSubType:create")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (expenseSubTypeEjb.isDuplicateNameOnSave(expenseSubType.getName())) {
            validateMsg = DUPLICATE_RECORD;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isVaildOnEdit() {
        if (!currentUser.isPermitted("expenseSubType:edit")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (expenseSubTypeEjb.isDuplicateNameOnEdit(expenseSubType)) {
            validateMsg = DUPLICATE_RECORD;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void deleteAction() {
        try {
            if (expenseSubType != null) {
                if (isCanDelete()) {
                    expenseSubTypeEjb.delete(expenseSubType);
                    JsfUtil.addSuccessMessage(DELETE_MSG);
                    newAction();
                    searchAction();
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
            e.printStackTrace();
        }
    }

    @Override
    public void onDeleteBtnClick() {
        RequestContext context = RequestContext.getCurrentInstance();
        if (expenseSubType.getId() != null) {
            context.execute("dlgDeleteConfirm.show()");
        } else {
            context.update("frmData:msg");
            JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
        }
    }

    @Override
    public boolean isCanDelete() {
        if (!currentUser.isPermitted("expenseSubType:delete")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (expenseEjb.findByExpenseSubType(expenseSubType).size() > 0
                | recurringExpenseEjb.findByExpenseSubType(expenseSubType).size() > 0) {
            validateMsg = PLEASE_DELETE_CHILD_DATA;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void searchAction() {
        expenseSubTypeList = expenseSubTypeEjb.findAll();
        expenseSubTypeDataModel = new ExpenseSubTypeDataModel(expenseSubTypeList);
    }

    @Override
    public void onRecordSelect(SelectEvent evt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /*
     NEW SECTION ###########################################################
     */
    public void searchByMainType() {
        System.out.println(""+searchExpenseMainType);
        if (searchExpenseMainType == null) {
            expenseSubTypeList = expenseSubTypeEjb.findAll();
        } else {
            expenseSubTypeList = expenseSubTypeEjb.findByExpenseMainType(searchExpenseMainType);
        }
        expenseSubTypeDataModel = new ExpenseSubTypeDataModel(expenseSubTypeList);
    }

    /*
     GETTERS AND SETTERS ####################################################
     */
    public ExpenseSubType getExpenseSubType() {
        return expenseSubType;
    }

    public void setExpenseSubType(ExpenseSubType expenseSubType) {
        this.expenseSubType = expenseSubType;
    }

    public List<ExpenseSubType> getExpenseSubTypeList() {
        return expenseSubTypeList;
    }

    public void setExpenseSubTypeList(List<ExpenseSubType> expenseSubTypeList) {
        this.expenseSubTypeList = expenseSubTypeList;
    }

    public ExpenseSubTypeDataModel getExpenseSubTypeDataModel() {
        return expenseSubTypeDataModel;
    }

    public void setExpenseSubTypeDataModel(ExpenseSubTypeDataModel expenseSubTypeDataModel) {
        this.expenseSubTypeDataModel = expenseSubTypeDataModel;
    }

    public ExpenseSubTypeEjb getExpenseSubTypeEjb() {
        return expenseSubTypeEjb;
    }

    public void setExpenseSubTypeEjb(ExpenseSubTypeEjb expenseSubTypeEjb) {
        this.expenseSubTypeEjb = expenseSubTypeEjb;
    }

    public String getValidateMsg() {
        return validateMsg;
    }

    public void setValidateMsg(String validateMsg) {
        this.validateMsg = validateMsg;
    }

    public ExpenseMainType getSearchExpenseMainType() {
        return searchExpenseMainType;
    }

    public void setSearchExpenseMainType(ExpenseMainType searchExpenseMainType) {
        this.searchExpenseMainType = searchExpenseMainType;
    }

}
