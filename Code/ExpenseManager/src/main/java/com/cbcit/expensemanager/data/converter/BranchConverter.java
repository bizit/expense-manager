/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.data.converter;

import com.cbcit.expensemanager.dao.BranchFacade;
import com.cbcit.expensemanager.domains.Branch;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Amith Fernando
 */
@ManagedBean
@FacesConverter(value = "branchConverter")
public class BranchConverter implements Converter {

    @EJB
    private BranchFacade facade;

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            if (value == null || value.isEmpty()) {
                return null;
            }
            if (!value.matches("\\d+")) {
                throw new ConverterException("The value is not a valid ID number: " + value);
            }
            return facade.find(Integer.valueOf(value));
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null && value instanceof Branch) {
            Branch object = (Branch) value;
            return "" + object.getId();
        }
        return "";
    }
}
