/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.expense.mb;

import com.amithfernando.web.mb.AbstractSecurityCheckMB;
import com.amithfernando.web.mb.CrudManagedBean;
import static com.amithfernando.web.mb.CrudManagedBean.DELETE_MSG;
import static com.amithfernando.web.mb.CrudManagedBean.NO_PERMISSION;
import static com.amithfernando.web.mb.CrudManagedBean.PLEASE_SELECT_RECORD;
import static com.amithfernando.web.mb.CrudManagedBean.SAVE_MSG;
import static com.amithfernando.web.mb.CrudManagedBean.UPDATE_MSG;
import com.amithfernando.web.util.JsfUtil;
import com.cbcit.expensemanager.business.expense.ejb.ExpensePaymentEjb;
import com.cbcit.expensemanager.business.expense.ejb.ExpenseRefundEjb;
import com.cbcit.expensemanager.data.model.ExpenseRefundDataModel;
import com.cbcit.expensemanager.domains.Expense;
import com.cbcit.expensemanager.domains.ExpensePayment;
import com.cbcit.expensemanager.domains.ExpenseRefund;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Amith
 */
@ManagedBean
@ViewScoped
public class ExpenseRefundMB extends AbstractSecurityCheckMB implements CrudManagedBean, Serializable {

    private ExpenseRefund expenseRefund;
    private List<ExpenseRefund> expenseRefundList;
    private List<ExpensePayment> expensePaymentList;
    private ExpenseRefundDataModel expenseRefundDataModel;
    @EJB
    private ExpenseRefundEjb expenseRefundEjb;
    @EJB
    private ExpensePaymentEjb expensePaymentEjb;
    //
    private String validateMsg;

    @PostConstruct
    public void init() {
        newAction();
        searchAction();
        //
        initSecurityCheck();
        //
        searchExpensePayment();
    }

    public void searchExpensePayment() {
        expensePaymentList = expensePaymentEjb.findAll();
    }

    @Override
    public void newAction() {
        expenseRefund = new ExpenseRefund();
        expenseRefundList = new ArrayList<ExpenseRefund>();
        expensePaymentList = new ArrayList<ExpensePayment>();
    }

    @Override
    public void saveAction() {
        try {
            if (expenseRefund.getId() == null) {
                if (isValidOnSave()) {
                    expenseRefundEjb.save(expenseRefund);
                    JsfUtil.addSuccessMessage(SAVE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                if (isVaildOnEdit()) {
                    expenseRefundEjb.edit(expenseRefund);
                    JsfUtil.addSuccessMessage(UPDATE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            }
            onSelectExpensePayment();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValidOnSave() {
        List<ExpenseRefund> expenseRefunds = expenseRefundEjb.findByExpensePayment(expenseRefund.getExpensePayment());
        double refundedAmount = 0;
        for (ExpenseRefund expenseRefund1 : expenseRefunds) {
            refundedAmount += expenseRefund1.getAmount();
        }
        if (!currentUser.isPermitted("expenseRefund:create")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (expenseRefund.getExpensePayment().getAmount() < expenseRefund.getAmount() + refundedAmount | expenseRefund.getAmount() <= 0) {
            validateMsg = "Pls check amount";
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isVaildOnEdit() {
        List<ExpenseRefund> expenseRefunds = expenseRefundEjb.findByExpensePayment(expenseRefund.getExpensePayment());
        double refundedAmount = 0;
        for (ExpenseRefund expenseRefund1 : expenseRefunds) {
            refundedAmount += expenseRefund1.getAmount();
        }
        if (!currentUser.isPermitted("expenseRefund:edit")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (expenseRefund.getExpensePayment().getAmount() < expenseRefund.getAmount() + refundedAmount | expenseRefund.getAmount() <= 0) {
            validateMsg = "Pls check amount";
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void deleteAction() {
        try {
            if (expenseRefund != null) {
                if (isCanDelete()) {
                    expenseRefundEjb.delete(expenseRefund);
                    JsfUtil.addSuccessMessage(DELETE_MSG);
                    newAction();
                    searchAction();
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
            e.printStackTrace();
        }
    }

    @Override
    public void onDeleteBtnClick() {
        RequestContext context = RequestContext.getCurrentInstance();
        if (expenseRefund.getId() != null) {
            context.execute("dlgDeleteConfirm.show()");
        } else {
            context.update("frmData:msg");
            JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
        }
    }

    @Override
    public boolean isCanDelete() {
        if (!currentUser.isPermitted("expenseRefund:delete")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void searchAction() {
    }

    @Override
    public void onRecordSelect(SelectEvent evt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    /*
     NEW SETION ##############################################################
     */

    public void onSelectJob() {
        expenseRefund = new ExpenseRefund();
        expensePaymentList = expensePaymentEjb.findAll();
        for (ExpensePayment expensePayment : expensePaymentList) {
            Expense expense = expensePayment.getExpenseid();
            List<ExpensePayment> expensePayments = expensePaymentEjb.findByExpense(expense);
            double balanceAmount = 0;
            for (ExpensePayment expensePayment1 : expensePayments) {
                balanceAmount += expensePayment1.getAmount();
            }
            expensePayment.getExpenseid().setExpenseBalanceAmount(expense.getExpenseAmount() - balanceAmount);
        }
        expenseRefundList = new ArrayList<ExpenseRefund>();
    }

    public void onSelectExpensePayment() {
        expenseRefundList = expenseRefundEjb.findByExpensePayment(expenseRefund.getExpensePayment());
        for (ExpenseRefund expenseRefund1 : expenseRefundList) {
            Expense expense = expenseRefund1.getExpensePayment().getExpenseid();
            List<ExpensePayment> expensePayments = expensePaymentEjb.findByExpense(expense);
            double balanceAmount = 0;
            for (ExpensePayment expensePayment1 : expensePayments) {
                balanceAmount += expensePayment1.getAmount();
            }
            expenseRefund1.getExpensePayment().getExpenseid().setExpenseBalanceAmount(expense.getExpenseAmount() - balanceAmount);
        }
    }

    /*
     *GETTER & SETTER ###########################################################
     */
    public ExpenseRefund getExpenseRefund() {
        return expenseRefund;
    }

    public void setExpenseRefund(ExpenseRefund expenseRefund) {
        this.expenseRefund = expenseRefund;
    }

    public List<ExpenseRefund> getExpenseRefundList() {
        return expenseRefundList;
    }

    public void setExpenseRefundList(List<ExpenseRefund> expenseRefundList) {
        this.expenseRefundList = expenseRefundList;
    }

    public ExpenseRefundDataModel getExpenseRefundDataModel() {
        return expenseRefundDataModel;
    }

    public void setExpenseRefundDataModel(ExpenseRefundDataModel expenseRefundDataModel) {
        this.expenseRefundDataModel = expenseRefundDataModel;
    }

    public ExpenseRefundEjb getExpenseRefundEjb() {
        return expenseRefundEjb;
    }

    public void setExpenseRefundEjb(ExpenseRefundEjb expenseRefundEjb) {
        this.expenseRefundEjb = expenseRefundEjb;
    }

    public String getValidateMsg() {
        return validateMsg;
    }

    public void setValidateMsg(String validateMsg) {
        this.validateMsg = validateMsg;
    }

    public List<ExpensePayment> getExpensePaymentList() {
        return expensePaymentList;
    }

    public void setExpensePaymentList(List<ExpensePayment> expensePaymentList) {
        this.expensePaymentList = expensePaymentList;
    }

}
