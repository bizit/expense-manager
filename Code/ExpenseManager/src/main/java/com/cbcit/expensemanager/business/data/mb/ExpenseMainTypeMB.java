/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.data.mb;

import com.amithfernando.web.mb.AbstractSecurityCheckMB;
import com.amithfernando.web.mb.CrudManagedBean;
import static com.amithfernando.web.mb.CrudManagedBean.DELETE_MSG;
import static com.amithfernando.web.mb.CrudManagedBean.DUPLICATE_RECORD;
import static com.amithfernando.web.mb.CrudManagedBean.NO_PERMISSION;
import static com.amithfernando.web.mb.CrudManagedBean.PLEASE_SELECT_RECORD;
import static com.amithfernando.web.mb.CrudManagedBean.SAVE_MSG;
import static com.amithfernando.web.mb.CrudManagedBean.UPDATE_MSG;
import com.amithfernando.web.util.JsfUtil;
import com.cbcit.expensemanager.business.data.ejb.ExpenseMainTypeEjb;
import com.cbcit.expensemanager.business.data.ejb.ExpenseSubTypeEjb;
import com.cbcit.expensemanager.data.model.ExpenseMainTypeDataModel;
import com.cbcit.expensemanager.domains.ExpenseMainType;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Amith
 */
@ManagedBean
@ViewScoped
public class ExpenseMainTypeMB extends AbstractSecurityCheckMB implements CrudManagedBean, Serializable {

    private ExpenseMainType expenseMainType;
    private List<ExpenseMainType> expenseMainTypeList;
    private ExpenseMainTypeDataModel expenseMainTypeDataModel;
    @EJB
    private ExpenseMainTypeEjb expenseMainTypeEjb;
    
    @EJB
    private ExpenseSubTypeEjb expenseSubTypeEjb;
    //
    private String validateMsg;

    @PostConstruct
    public void init() {
        newAction();
        searchAction();
        //
        initSecurityCheck();
    }

    @Override
    public void newAction() {
        expenseMainType = new ExpenseMainType();
    }

    @Override
    public void saveAction() {
        try {
            if (expenseMainType.getId() == null) {
                if (isValidOnSave()) {
                    expenseMainTypeEjb.save(expenseMainType);
                    JsfUtil.addSuccessMessage(SAVE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                if (isVaildOnEdit()) {
                    expenseMainTypeEjb.edit(expenseMainType);
                    JsfUtil.addSuccessMessage(UPDATE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            }
            searchAction();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValidOnSave() {
        if (!currentUser.isPermitted("expenseMainType:create")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (expenseMainTypeEjb.isDuplicateNameOnSave(expenseMainType.getName())) {
            validateMsg = DUPLICATE_RECORD;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isVaildOnEdit() {
        if (!currentUser.isPermitted("expenseMainType:edit")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (expenseMainTypeEjb.isDuplicateNameOnEdit(expenseMainType)) {
            validateMsg = DUPLICATE_RECORD;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void deleteAction() {
        try {
            if (expenseMainType != null) {
                if (isCanDelete()) {
                    expenseMainTypeEjb.delete(expenseMainType);
                    JsfUtil.addSuccessMessage(DELETE_MSG);
                    newAction();
                    searchAction();
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
            e.printStackTrace();
        }
    }

    @Override
    public void onDeleteBtnClick() {
        RequestContext context = RequestContext.getCurrentInstance();
        if (expenseMainType.getId() != null) {
            context.execute("dlgDeleteConfirm.show()");
        } else {
            context.update("frmData:msg");
            JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
        }
    }

    @Override
    public boolean isCanDelete() {
        if (!currentUser.isPermitted("expenseMainType:delete")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if(expenseSubTypeEjb.findByExpenseMainType(expenseMainType).size()>0){
            validateMsg = PLEASE_DELETE_CHILD_DATA;
            return false;
        }else {
            return true;
        }
    }

    @Override
    public void searchAction() {
        expenseMainTypeList = expenseMainTypeEjb.findAll();
        expenseMainTypeDataModel = new ExpenseMainTypeDataModel(expenseMainTypeList);
    }

    @Override
    public void onRecordSelect(SelectEvent evt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /*
     GETTERS AND SETTERS ####################################################
     */
    public ExpenseMainType getExpenseMainType() {
        return expenseMainType;
    }

    public void setExpenseMainType(ExpenseMainType expenseMainType) {
        this.expenseMainType = expenseMainType;
    }

    public List<ExpenseMainType> getExpenseMainTypeList() {
        return expenseMainTypeList;
    }

    public void setExpenseMainTypeList(List<ExpenseMainType> expenseMainTypeList) {
        this.expenseMainTypeList = expenseMainTypeList;
    }

    public ExpenseMainTypeEjb getExpenseMainTypeEjb() {
        return expenseMainTypeEjb;
    }

    public void setExpenseMainTypeEjb(ExpenseMainTypeEjb expenseMainTypeEjb) {
        this.expenseMainTypeEjb = expenseMainTypeEjb;
    }

}
