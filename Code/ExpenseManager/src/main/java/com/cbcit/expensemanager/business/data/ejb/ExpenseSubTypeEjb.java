/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cbcit.expensemanager.business.data.ejb;

import com.amithfernando.web.exception.CreateException;
import com.cbcit.expensemanager.dao.ExpenseSubTypeFacade;
import com.cbcit.expensemanager.domains.ExpenseMainType;
import com.cbcit.expensemanager.domains.ExpenseSubType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 *
 * @author Amith
 */
@Stateless
public class ExpenseSubTypeEjb {

    @EJB
    private ExpenseSubTypeFacade expenseSubTypeFacade;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void save(ExpenseSubType expenseSubType) throws CreateException {
        expenseSubTypeFacade.create(expenseSubType);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void edit(ExpenseSubType expenseSubType) throws CreateException {
        expenseSubTypeFacade.edit(expenseSubType);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void delete(ExpenseSubType expenseSubType) throws CreateException {
        expenseSubTypeFacade.remove(expenseSubType);
    }

    public List<ExpenseSubType> findAll() {
        return expenseSubTypeFacade.findAll();
    }

    public ExpenseSubType findByName(String name) {
        String sql = "SELECT a FROM ExpenseSubType a WHERE a.name=:name";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", name);
        return expenseSubTypeFacade.findbyQuerySingle(sql, params);
    }

    public boolean isDuplicateNameOnSave(String name) {
        ExpenseSubType object = findByName(name);
        if (object != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isDuplicateNameOnEdit(ExpenseSubType newObject) {
        boolean isDuplicate;
        ExpenseSubType oldObject = expenseSubTypeFacade.find(newObject.getId());
        if (oldObject.getName().equals(newObject.getName())) {
            isDuplicate = false;
        } else {
            isDuplicate = isDuplicateNameOnSave(newObject.getName());
        }
        return isDuplicate;
    }
        
     
    /*
    *QUERY AREA ################################################################
    */
    
    public List<ExpenseSubType> findByExpenseMainType(ExpenseMainType expenseMainType) {
        String sql = "SELECT a FROM ExpenseSubType a WHERE a.expenseMainType=:expenseMainType";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expenseMainType", expenseMainType);
        return expenseSubTypeFacade.findbyQuery(sql, params);
    }
}
