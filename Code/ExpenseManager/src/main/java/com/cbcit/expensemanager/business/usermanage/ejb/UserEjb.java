package com.cbcit.expensemanager.business.usermanage.ejb;

import com.amithfernando.web.exception.CreateException;
import com.cbcit.expensemanager.dao.UserFacade;
import com.cbcit.expensemanager.dao.UserPermissionFacade;
import com.cbcit.expensemanager.domains.Permission;
import com.cbcit.expensemanager.domains.User;
import com.cbcit.expensemanager.domains.UserPermission;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 *
 * @author Amith Fernando
 */
@Stateless
public class UserEjb {

    @EJB
    private UserFacade userFacade;
    @EJB
    private UserPermissionFacade userPermissionFacade;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void save(User user, List<Permission> permissionList) throws CreateException {
        userFacade.create(user);
        //
        for (Permission permission : permissionList) {
            UserPermission userPermission = new UserPermission();
            userPermission.setPermission(permission);
            userPermission.setUser(user);
            userPermissionFacade.create(userPermission);
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void edit(User user, List<Permission> permissionList) throws CreateException {
        user.setModifiedDate(new Date());
        userFacade.edit(user);
        //
        deleteUserPermissions(user);
        //
        for (Permission permission : permissionList) {
            UserPermission userPermission = new UserPermission();
            userPermission.setPermission(permission);
            userPermission.setUser(user);
            userPermissionFacade.create(userPermission);
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void setUserLastlogin(User user) throws CreateException {
        user.setLastLogin(new Date());
        userFacade.edit(user);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void chnagePassword(User user) throws CreateException {
        user.setModifiedDate(new Date());
        userFacade.edit(user);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private void deleteUserPermissions(User user) {
        List<UserPermission> userPermissions = findByUser(user);
        for (UserPermission userPermission : userPermissions) {
            userPermissionFacade.remove(userPermission);
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void delete(User user) throws CreateException {
        deleteUserPermissions(user);
        //
        userFacade.remove(user);
    }

    /*
     * #########################################################################
     */
    public List<User> findAll() {
        return userFacade.findAll();
    }

    public List<UserPermission> findByUser(User user) {
        String sql = "SELECT a FROM UserPermission a WHERE a.user = :user";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user", user);
        return userPermissionFacade.findbyQuery(sql, params);
    }

    public User findByUserName(String userName) {
        String sql = "SELECT a FROM User a WHERE a.userName = :userName";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("userName", userName);
        return userFacade.findbyQuerySingle(sql, params);
    }
}
