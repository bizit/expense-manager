/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.data.model;


import com.cbcit.expensemanager.domains.ExpensePayment;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Amith Fernando
 */
public class ExpensePaymentDataModel extends ListDataModel<ExpensePayment> implements SelectableDataModel<ExpensePayment>, Serializable {

    public ExpensePaymentDataModel() {
    }

    public ExpensePaymentDataModel(List<ExpensePayment> object) {
        super(object);
    }

    @Override
    public ExpensePayment getRowData(String rowKey) {
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  

        List<ExpensePayment> objectList = (List<ExpensePayment>) getWrappedData();

        for (ExpensePayment object : objectList) {
            if (object.getId().equals(rowKey)) {
                return object;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(ExpensePayment object) {
        return object.getId();
    }
}
