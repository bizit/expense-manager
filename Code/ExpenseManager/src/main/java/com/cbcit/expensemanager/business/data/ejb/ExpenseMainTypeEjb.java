/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cbcit.expensemanager.business.data.ejb;

import com.amithfernando.web.exception.CreateException;
import com.cbcit.expensemanager.dao.ExpenseMainTypeFacade;
import com.cbcit.expensemanager.domains.ExpenseMainType;
import com.cbcit.expensemanager.domains.ExpenseSubType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 *
 * @author Amith
 */
@Stateless
public class ExpenseMainTypeEjb {

    @EJB
    private ExpenseMainTypeFacade expenseMainTypeFacade;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void save(ExpenseMainType expenseMainType) throws CreateException {
        expenseMainTypeFacade.create(expenseMainType);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void edit(ExpenseMainType expenseMainType) throws CreateException {
        expenseMainTypeFacade.edit(expenseMainType);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void delete(ExpenseMainType expenseMainType) throws CreateException {
        expenseMainTypeFacade.remove(expenseMainType);
    }

    public List<ExpenseMainType> findAll() {
        return expenseMainTypeFacade.findAll();
    }

    public ExpenseMainType findByName(String name) {
        String sql = "SELECT a FROM ExpenseMainType a WHERE a.name=:name";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", name);
        return expenseMainTypeFacade.findbyQuerySingle(sql, params);
    }

    public boolean isDuplicateNameOnSave(String name) {
        ExpenseMainType object = findByName(name);
        if (object != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isDuplicateNameOnEdit(ExpenseMainType newObject) {
        boolean isDuplicate;
        ExpenseMainType oldObject = expenseMainTypeFacade.find(newObject.getId());
        if (oldObject.getName().equals(newObject.getName())) {
            isDuplicate = false;
        } else {
            isDuplicate = isDuplicateNameOnSave(newObject.getName());
        }
        return isDuplicate;
    }
}
