/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.data.model;


import com.cbcit.expensemanager.domains.Branch;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Amith Fernando
 */
public class BranchDataModel extends ListDataModel<Branch> implements SelectableDataModel<Branch>, Serializable {

    public BranchDataModel() {
    }

    public BranchDataModel(List<Branch> object) {
        super(object);
    }

    @Override
    public Branch getRowData(String rowKey) {
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  

        List<Branch> objectList = (List<Branch>) getWrappedData();

        for (Branch object : objectList) {
            if (object.getId().equals(rowKey)) {
                return object;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(Branch object) {
        return object.getId();
    }
}
