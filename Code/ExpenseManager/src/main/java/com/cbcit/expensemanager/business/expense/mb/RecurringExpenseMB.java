/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.expense.mb;

import com.amithfernando.web.mb.AbstractSecurityCheckMB;
import com.amithfernando.web.mb.CrudManagedBean;
import static com.amithfernando.web.mb.CrudManagedBean.PLEASE_SELECT_RECORD;
import static com.amithfernando.web.mb.CrudManagedBean.SAVE_MSG;
import static com.amithfernando.web.mb.CrudManagedBean.UPDATE_MSG;
import com.amithfernando.web.util.JsfUtil;
import com.cbcit.expensemanager.business.data.ejb.ExpenseSubTypeEjb;
import com.cbcit.expensemanager.business.expense.ejb.RecurringExpenseEjb;
import com.cbcit.expensemanager.data.model.RecurringExpenseDataModel;
import com.cbcit.expensemanager.domains.ExpenseMainType;
import com.cbcit.expensemanager.domains.ExpenseSubType;
import com.cbcit.expensemanager.domains.RecurringExpense;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Kalana Sarange
 */
@ManagedBean
@ViewScoped
public class RecurringExpenseMB extends AbstractSecurityCheckMB implements CrudManagedBean, Serializable {

    private RecurringExpense recurringExpense;
    private List<RecurringExpense> recurringExpenseList;
    private RecurringExpenseDataModel recurringExpenseDataModel;
    private ExpenseMainType expenseMainType;
    private List<ExpenseSubType> expenseSubTypeList;
    
    
    @EJB
    private RecurringExpenseEjb recurringExpenseEjb;
    
    @EJB
    private ExpenseSubTypeEjb expenseSubTypeEjb;
    //
    private String validateMsg;    
    
    @PostConstruct
    public void init() {
        newAction();
        searchAction();
        //
        initSecurityCheck();
    }

    @Override
    public void newAction() {
        recurringExpense = new RecurringExpense();
    }

    @Override
    public void saveAction() {
        try {
            if (recurringExpense.getId() == null) {
                if (isValidOnSave()) {
                    recurringExpenseEjb.save(recurringExpense);
                    JsfUtil.addSuccessMessage(SAVE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                if (isVaildOnEdit()) {
                    recurringExpenseEjb.edit(recurringExpense);
                    JsfUtil.addSuccessMessage(UPDATE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            }
            searchAction();
        } catch (Exception e) {
            e.printStackTrace();
            JsfUtil.addErrorMessage("Error " + e);
        }
    }

    @Override
    public boolean isValidOnSave() {
        if (!currentUser.isPermitted("recurringExpense:create")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (recurringExpenseEjb.isDuplicateNameOnSave(recurringExpense.getName())) {
            System.out.println("duplicate");
            validateMsg = DUPLICATE_RECORD;
            return false;
        }else {
            return true;
        }
    }

    @Override
    public boolean isVaildOnEdit() {
        if (!currentUser.isPermitted("recurringExpense:edit")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (recurringExpenseEjb.isDuplicateNameOnEdit(recurringExpense)) {
            validateMsg = DUPLICATE_RECORD;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void deleteAction() {
        try {
            if (recurringExpense != null) {
                if (isCanDelete()) {
                    recurringExpenseEjb.delete(recurringExpense);
                    JsfUtil.addSuccessMessage(DELETE_MSG);
                    newAction();
                    searchAction();
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
            e.printStackTrace();
        }
    }
    
       @Override
    public void onDeleteBtnClick() {
        RequestContext context = RequestContext.getCurrentInstance();
        if (recurringExpense.getId() != null) {
            context.execute("dlgDeleteConfirm.show()");
        } else {
            context.update("frmData:msg");
            JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
        }
    }

    @Override
    public boolean isCanDelete() {
        if (!currentUser.isPermitted("recurringExpense:delete")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if(!recurringExpenseEjb.isCanDelete(recurringExpense)) {
            validateMsg = PLEASE_DELETE_CHILD_DATA;
            return false;
        }else{
            return true;
        }
    }

    @Override
    public void searchAction() {
        recurringExpenseList = recurringExpenseEjb.findAll();
        recurringExpenseDataModel = new RecurringExpenseDataModel(recurringExpenseList);
    }

    public void onMainTypeChanged() {
        if (expenseMainType != null) {
            expenseSubTypeList = expenseSubTypeEjb.findByExpenseMainType(expenseMainType);
        }
    }
    
    @Override
    public void onRecordSelect(SelectEvent evt) {
        expenseMainType = recurringExpense.getExpenseSubType().getExpenseMainType();
        expenseSubTypeList = expenseSubTypeEjb.findByExpenseMainType(expenseMainType);
    }

    /*
     * GETTER & SETTER #########################################################
     */
    public RecurringExpense getRecurringExpense() {
        return recurringExpense;
    }

    public void setRecurringExpense(RecurringExpense recurringExpense) {
        this.recurringExpense = recurringExpense;
    }

    public List<RecurringExpense> getRecurringExpenseList() {
        return recurringExpenseList;
    }

    public void setRecurringExpenseList(List<RecurringExpense> recurringExpenseList) {
        this.recurringExpenseList = recurringExpenseList;
    }

    public RecurringExpenseDataModel getRecurringExpenseDataModel() {
        return recurringExpenseDataModel;
    }

    public void setRecurringExpenseDataModel(RecurringExpenseDataModel recurringExpenseDataModel) {
        this.recurringExpenseDataModel = recurringExpenseDataModel;
    }

    public RecurringExpenseEjb getRecurringExpenseEjb() {
        return recurringExpenseEjb;
    }

    public void setRecurringExpenseEjb(RecurringExpenseEjb recurringExpenseEjb) {
        this.recurringExpenseEjb = recurringExpenseEjb;
    }

    public String getValidateMsg() {
        return validateMsg;
    }

    public void setValidateMsg(String validateMsg) {
        this.validateMsg = validateMsg;
    }

    public ExpenseMainType getExpenseMainType() {
        return expenseMainType;
    }

    public void setExpenseMainType(ExpenseMainType expenseMainType) {
        this.expenseMainType = expenseMainType;
    }

    public List<ExpenseSubType> getExpenseSubTypeList() {
        return expenseSubTypeList;
    }

    public void setExpenseSubTypeList(List<ExpenseSubType> expenseSubTypeList) {
        this.expenseSubTypeList = expenseSubTypeList;
    }
    
    
}
