/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.expense.mb;

import com.amithfernando.web.mb.AbstractSecurityCheckMB;
import com.amithfernando.web.mb.CrudManagedBean;
import com.amithfernando.web.util.JsfUtil;
import com.cbcit.expensemanager.business.expense.ejb.ExpenseEjb;
import com.cbcit.expensemanager.business.expense.ejb.ExpensePaymentEjb;
import com.cbcit.expensemanager.business.expense.ejb.ExpenseRefundEjb;
import com.cbcit.expensemanager.data.model.ExpensePaymentDataModel;
import com.cbcit.expensemanager.domains.Expense;
import com.cbcit.expensemanager.domains.ExpensePayment;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Amith
 */
@ManagedBean
@ViewScoped
public class ExpensePaymentMB extends AbstractSecurityCheckMB implements CrudManagedBean, Serializable {


    private ExpensePayment expensePayment;
    private List<Expense> expenseList;
    private List<ExpensePayment> expensePaymentList;
    private ExpensePaymentDataModel expensePaymentDataModel;
    @EJB
    private ExpensePaymentEjb expensePaymentEjb;
    @EJB
    private ExpenseEjb expenseEjb;
    @EJB
    private ExpenseRefundEjb expenseRefundEjb;
    //
    private String validateMsg;
    //
    private double payableAmount;
    private double paidAmount;
    private double dueAmount;

    @PostConstruct
    public void init() {
        newAction();
        searchAction();
        //
        initSecurityCheck();
        //
        searchExpenses();
    }
    
    public void searchExpenses(){
        expenseList=expenseEjb.findAll();
    }

    @Override
    public void newAction() {
        expensePayment = new ExpensePayment();
        expenseList = new ArrayList<Expense>();
        expensePaymentList = new ArrayList<ExpensePayment>();
        paidAmount = 0;
        dueAmount = 0;
        payableAmount = 0;
    }

    @Override
    public void saveAction() {
        try {
            if (expensePayment.getId() == null) {
                if (isValidOnSave()) {
                    expensePaymentEjb.save(expensePayment);
                    JsfUtil.addSuccessMessage(SAVE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                if (isVaildOnEdit()) {
                    expensePaymentEjb.edit(expensePayment);
                    JsfUtil.addSuccessMessage(UPDATE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            }
            onSelectExpense();
            setSummary();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValidOnSave() {
        List<ExpensePayment> expensePaymentList=expensePaymentEjb.findByExpense(expensePayment.getExpenseid());
        double totalExpensePaid=0;
        for (ExpensePayment expensePayment1 : expensePaymentList) {
            totalExpensePaid+=expensePayment1.getAmount();
        }
        if (!currentUser.isPermitted("expensePayment:create")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (expensePayment.getAmount() <= 0 | totalExpensePaid+expensePayment.getAmount()>expensePayment.getExpenseid().getExpenseAmount()) {
            validateMsg = "Pls check amount";
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isVaildOnEdit() {
        if (!currentUser.isPermitted("expensePayment:edit")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (!expenseRefundEjb.findByExpensePayment(expensePayment).isEmpty()) {
            validateMsg = PLEASE_DELETE_CHILD_DATA;
            return false;
        } else if (expensePayment.getAmount() <= 0) {
            validateMsg = "Pls check amount";
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void deleteAction() {
        try {
            if (expensePayment != null) {
                if (isCanDelete()) {
                    expensePaymentEjb.delete(expensePayment);
                    JsfUtil.addSuccessMessage(DELETE_MSG);
                    newAction();
                    searchAction();
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
            e.printStackTrace();
        }
    }

    @Override
    public void onDeleteBtnClick() {
        RequestContext context = RequestContext.getCurrentInstance();
        if (expensePayment.getId() != null) {
            context.execute("dlgDeleteConfirm.show()");
        } else {
            context.update("frmData:msg");
            JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
        }
    }

    @Override
    public boolean isCanDelete() {
        if (!currentUser.isPermitted("expensePayment:delete")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if(!expenseRefundEjb.findByExpensePayment(expensePayment).isEmpty()) {
            validateMsg = PLEASE_DELETE_CHILD_DATA;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void searchAction() {
    }

    @Override
    public void onRecordSelect(SelectEvent evt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    /*
     NEW SETION #############################################################
     */


    public void onSelectExpense() {
        expensePaymentList = new ArrayList<ExpensePayment>();
        expensePaymentList = expensePaymentEjb.findByExpense(expensePayment.getExpenseid());
    }

    public void setSummary() {
        double paid = 0;
        double payable = 0;
        List<ExpensePayment> paidExpenseList = expensePaymentEjb.findByExpense(expensePayment.getExpenseid());
        for (ExpensePayment expensePayment1 : paidExpenseList) {
            paid += expensePayment1.getAmount();
        }
        List<Expense> expenseList = expenseEjb.findAll();
        for (Expense expense : expenseList) {
            payable += expense.getExpenseAmount();
        }
        payableAmount = payable;
        paidAmount = paid;
        dueAmount = payableAmount - paidAmount;
    }

    /*
     *GETTER & SETTER ###########################################################
     */
    public ExpensePayment getExpensePayment() {
        return expensePayment;
    }

    public void setExpensePayment(ExpensePayment expensePayment) {
        this.expensePayment = expensePayment;
    }

    public List<ExpensePayment> getExpensePaymentList() {
        return expensePaymentList;
    }

    public void setExpensePaymentList(List<ExpensePayment> expensePaymentList) {
        this.expensePaymentList = expensePaymentList;
    }

    public ExpensePaymentDataModel getExpensePaymentDataModel() {
        return expensePaymentDataModel;
    }

    public void setExpensePaymentDataModel(ExpensePaymentDataModel expensePaymentDataModel) {
        this.expensePaymentDataModel = expensePaymentDataModel;
    }

    public ExpensePaymentEjb getExpensePaymentEjb() {
        return expensePaymentEjb;
    }

    public void setExpensePaymentEjb(ExpensePaymentEjb expensePaymentEjb) {
        this.expensePaymentEjb = expensePaymentEjb;
    }

    public String getValidateMsg() {
        return validateMsg;
    }

    public void setValidateMsg(String validateMsg) {
        this.validateMsg = validateMsg;
    }

  

    public List<Expense> getExpenseList() {
        return expenseList;
    }

    public void setExpenseList(List<Expense> expenseList) {
        this.expenseList = expenseList;
    }

    public double getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(double payableAmount) {
        this.payableAmount = payableAmount;
    }

    public double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public double getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(double dueAmount) {
        this.dueAmount = dueAmount;
    }

}
