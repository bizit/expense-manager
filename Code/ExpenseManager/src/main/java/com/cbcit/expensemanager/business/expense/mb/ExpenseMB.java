/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.expense.mb;

import com.amithfernando.web.mb.AbstractSecurityCheckMB;
import com.amithfernando.web.mb.CrudManagedBean;
import static com.amithfernando.web.mb.CrudManagedBean.PLEASE_SELECT_RECORD;
import static com.amithfernando.web.mb.CrudManagedBean.SAVE_MSG;
import static com.amithfernando.web.mb.CrudManagedBean.UPDATE_MSG;
import com.amithfernando.web.util.JsfUtil;
import com.cbcit.expensemanager.business.data.ejb.ExpenseSubTypeEjb;
import com.cbcit.expensemanager.business.expense.ejb.ExpenseEjb;
import com.cbcit.expensemanager.business.expense.ejb.ExpensePaymentEjb;
import com.cbcit.expensemanager.data.model.ExpenseDataModel;
import com.cbcit.expensemanager.domains.Expense;
import com.cbcit.expensemanager.domains.ExpenseMainType;
import com.cbcit.expensemanager.domains.ExpenseSubType;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Amith
 */
@ManagedBean
@ViewScoped
public class ExpenseMB extends AbstractSecurityCheckMB implements CrudManagedBean, Serializable {

    @EJB
    private ExpenseEjb expenseEjb;
    private Expense expense;
    private List<Expense> expenseList;
    private ExpenseDataModel expenseDataModel;
    private String validateMsg;
    //
    @EJB
    private ExpenseSubTypeEjb expenseSubTypeEjb;
    @EJB
    private ExpensePaymentEjb expensePaymentEjb;
    private ExpenseMainType selectedExpenseMainType;
    private List<ExpenseSubType> expenseSubTypeList;


    @PostConstruct
    private void init() {
        initSecurityCheck();
        //
        newAction();
        searchAction();
    }

    @Override
    public void newAction() {
        expense = new Expense();
        expense.setExpenseDate(new Date());
        expense.setExpenseType("Regular");
        selectedExpenseMainType = new ExpenseMainType();
    }

    @Override
    public void saveAction() {
        try {
            if (expense.getId() == null) {
                if (isValidOnSave()) {
                    expenseEjb.save(expense);
                    JsfUtil.addSuccessMessage(SAVE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                if (isVaildOnEdit()) {
                    expenseEjb.edit(expense);
                    JsfUtil.addSuccessMessage(UPDATE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            }
            searchAction();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValidOnSave() {
        if (!currentUser.isPermitted("expense:create")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (expenseEjb.isDuplicateNoOnSave(expense.getExpenseNo())) {
            validateMsg = "Duplicate No";
            return false;
        } else if (expense.getExpenseAmount() <= 0) {
            validateMsg = "Pls check amount";
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isVaildOnEdit() {
        if (!currentUser.isPermitted("expense:edit")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (expenseEjb.isDuplicateNoOnEdit(expense)) {
            validateMsg = "Duplicate No";
            return false;
        } else if (expense.getExpenseAmount() <= 0) {
            validateMsg = "Pls check amount";
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void deleteAction() {
        try {
            if (expense != null) {
                if (isCanDelete()) {
                    expenseEjb.delete(expense);
                    JsfUtil.addSuccessMessage(DELETE_MSG);
                    newAction();
                    searchAction();
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
            e.printStackTrace();
        }
    }

    @Override
    public void onDeleteBtnClick() {
        RequestContext context = RequestContext.getCurrentInstance();
        if (expense.getId() != null) {
            context.execute("dlgDeleteConfirm.show()");
        } else {
            context.update("frmData:msg");
            JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
        }
    }

    @Override
    public boolean isCanDelete() {
         if (!currentUser.isPermitted("expense:delete")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if(!expensePaymentEjb.findByExpense(expense).isEmpty()) {
            validateMsg = PLEASE_DELETE_CHILD_DATA;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void searchAction() {
        expenseList = expenseEjb.findByType("Regular");
    }

    @Override
    public void onRecordSelect(SelectEvent evt) {
        //
    }

    /*
     *SECTION OTHER #############################################################
     */
    public void onExpenseMainTypeSelect() {
        expenseSubTypeList = expenseSubTypeEjb.findByExpenseMainType(selectedExpenseMainType);
    }


    public void onSelectExpense() {
        selectedExpenseMainType = expense.getExpenseSubType().getExpenseMainType();
        expenseSubTypeList = expenseSubTypeEjb.findByExpenseMainType(selectedExpenseMainType);
    }


    /*
     * GETTER & SETTER ##########################################################
     */
    public ExpenseMainType getSelectedExpenseMainType() {
        return selectedExpenseMainType;
    }

    public void setSelectedExpenseMainType(ExpenseMainType selectedExpenseMainType) {
        this.selectedExpenseMainType = selectedExpenseMainType;
    }

    public List<ExpenseSubType> getExpenseSubTypeList() {
        return expenseSubTypeList;
    }

    public void setExpenseSubTypeList(List<ExpenseSubType> expenseSubTypeList) {
        this.expenseSubTypeList = expenseSubTypeList;
    }

    public String getValidateMsg() {
        return validateMsg;
    }

    public void setValidateMsg(String validateMsg) {
        this.validateMsg = validateMsg;
    }

    public ExpenseEjb getExpenseEjb() {
        return expenseEjb;
    }

    public void setExpenseEjb(ExpenseEjb expenseEjb) {
        this.expenseEjb = expenseEjb;
    }

    public Expense getExpense() {
        return expense;
    }

    public void setExpense(Expense expense) {
        this.expense = expense;
    }

    public List<Expense> getExpenseList() {
        return expenseList;
    }

    public void setExpenseList(List<Expense> expenseList) {
        this.expenseList = expenseList;
    }

    public ExpenseDataModel getExpenseDataModel() {
        return expenseDataModel;
    }

    public void setExpenseDataModel(ExpenseDataModel expenseDataModel) {
        this.expenseDataModel = expenseDataModel;
    }


}
