/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.data.ejb;


import com.amithfernando.web.exception.CreateException;
import com.cbcit.expensemanager.dao.BranchFacade;
import com.cbcit.expensemanager.domains.Branch;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 *
 * @author Admin
 */
@Stateless
public class BranchEjb {

    @EJB
    private BranchFacade branchFacade;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void save(Branch branch) throws CreateException {
        branchFacade.create(branch);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void edit(Branch branch) throws CreateException {
        branchFacade.edit(branch);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void delete(Branch branch) throws CreateException {
        branchFacade.remove(branch);
    }

    public List<Branch> findAll() {
        return branchFacade.findAll();
    }

    public Branch findByName(String name) {
        String sql = "SELECT a FROM Branch a WHERE a.name=:name";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", name);
        return branchFacade.findbyQuerySingle(sql, params);
    }

    public boolean isDuplicateNameOnSave(String name) {
        Branch object = findByName(name);
        if (object != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isDuplicateNameOnEdit(Branch newObject) {
        boolean isDuplicate;
        Branch oldObject = branchFacade.find(newObject.getId());
        if (oldObject.getName().equals(newObject.getName())) {
            isDuplicate = false;
        } else {
            isDuplicate = isDuplicateNameOnSave(newObject.getName());
        }
        return isDuplicate;
    }
}
