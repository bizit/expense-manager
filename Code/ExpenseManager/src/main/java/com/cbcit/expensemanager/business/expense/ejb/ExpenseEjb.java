/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.expense.ejb;

import com.amithfernando.web.exception.CreateException;
import com.cbcit.expensemanager.dao.ExpenseFacade;
import com.cbcit.expensemanager.domains.Expense;
import com.cbcit.expensemanager.domains.ExpenseMainType;
import com.cbcit.expensemanager.domains.ExpensePayment;
import com.cbcit.expensemanager.domains.ExpenseRefund;
import com.cbcit.expensemanager.domains.ExpenseSubType;
import com.cbcit.expensemanager.domains.RecurringExpense;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 *
 * @author Amith
 */
@Stateless
public class ExpenseEjb {

    @EJB
    private ExpenseFacade expenseFacade;
    @EJB
    private ExpensePaymentEjb expensePaymentEjb;
    @EJB
    private ExpenseRefundEjb expenseRefundEjb;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void save(Expense expense) throws CreateException {
        expense.setExpenseNo(loadNextExpenseNo());
        expenseFacade.create(expense);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void edit(Expense expense) throws CreateException {
        expenseFacade.edit(expense);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void delete(Expense expense) throws CreateException {
        expenseFacade.remove(expense);
    }

    public List<Expense> findAll() {
        return expenseFacade.findAll();
    }

    public Expense findByName(String name) {
        String sql = "SELECT a FROM Expense a WHERE a.name=:name";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", name);
        return expenseFacade.findbyQuerySingle(sql, params);
    }

    public Expense findByExpenseNo(String expenseNo) {
        String sql = "SELECT a FROM Expense a WHERE a.expenseNo=:expenseNo";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expenseNo", expenseNo);
        return expenseFacade.findbyQuerySingle(sql, params);
    }

    public boolean isDuplicateNameOnSave(String name) {
        Expense object = findByName(name);
        if (object != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isDuplicateNameOnEdit(Expense newObject) {
        boolean isDuplicate;
        Expense oldObject = expenseFacade.find(newObject.getId());
        if (oldObject.getName().equals(newObject.getName())) {
            isDuplicate = false;
        } else {
            isDuplicate = isDuplicateNameOnSave(newObject.getName());
        }
        return isDuplicate;
    }

    public boolean isDuplicateExpenseNoOnSave(String no) {
        Expense object = findByExpenseNo(no);
        if (object != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isDuplicateExpenseNoOnEdit(Expense newObject) {
        boolean isDuplicate;
        Expense oldObject = expenseFacade.find(newObject.getId());
        if (oldObject.getExpenseNo().equals(newObject.getExpenseNo())) {
            isDuplicate = false;
        } else {
            isDuplicate = isDuplicateExpenseNoOnSave(newObject.getExpenseNo());
        }
        return isDuplicate;
    }

    /*
     *QUERY AREA ################################################################
     */
    public String loadNextExpenseNo() {
        String expenseNo = "1000000";
        String sql = "SELECT a FROM Expense a ORDER BY a.expenseNo DESC";
        Map<String, Object> params = new HashMap<String, Object>();
        List<Expense> c = expenseFacade.findbyQueryLimit(sql, params, 1);

        if (c.isEmpty()) {
            return expenseNo;
        } else {
            Expense expense = c.get(0);
            int previosiExpenseNo = Integer.parseInt(expense.getExpenseNo());
            expenseNo = (previosiExpenseNo + 1) + "";
            return expenseNo;
        }
    }
    
    public List<Expense> findByRecurngExpense(RecurringExpense recurringExpense) {
        String sql = "SELECT a FROM Expense a WHERE a.recurringExpense=:recurringExpense ";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("recurringExpense", recurringExpense);
        return expenseFacade.findbyQuery(sql, params);
    }

    public List<Expense> findByRecurngExpenseAndDate(RecurringExpense recurringExpense, Date expenseDate) {
        String sql = "SELECT a FROM Expense a WHERE a.recurringExpense=:recurringExpense AND a.expenseDate=:expenseDate";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("recurringExpense", recurringExpense);
        params.put("expenseDate", expenseDate);
        return expenseFacade.findbyQuery(sql, params);
    }

    public Expense findByNo(String no) {
        String sql = "SELECT a FROM Expense a WHERE a.expenseNo=:no";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("no", no);
        return expenseFacade.findbyQuerySingle(sql, params);
    }

    public boolean isDuplicateNoOnSave(String no) {
        Expense object = findByNo(no);
        if (object != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isDuplicateNoOnEdit(Expense newObject) {
        boolean isDuplicate;
        Expense oldObject = expenseFacade.find(newObject.getId());
        if (oldObject.getExpenseNo().equals(newObject.getExpenseNo())) {
            isDuplicate = false;
        } else {
            isDuplicate = isDuplicateNoOnSave(newObject.getName());
        }
        return isDuplicate;
    }

    /*
     SUMMARY ####################################################################
     */
    public Expense initExpense(Expense expense) {

        double expensePaidAmount = 0;
        double expenseRefundAmount = 0;
        double expenseBalanceAmount = 0;

        List<ExpensePayment> expensePayments = expensePaymentEjb.findByExpense(expense);
        for (ExpensePayment expensePayment : expensePayments) {
            expensePaidAmount += expensePayment.getAmount();
            List<ExpenseRefund> expenseRefunds = expenseRefundEjb.findByExpensePayment(expensePayment);
            for (ExpenseRefund expenseRefund : expenseRefunds) {
                expenseRefundAmount += expenseRefund.getAmount();
            }
        }
        expensePaidAmount = expensePaidAmount - expenseRefundAmount;
        expense.setExpensePaidAmount(expensePaidAmount);
        expenseBalanceAmount = expense.getExpenseAmount() - expensePaidAmount;
        expense.setExpenseBalanceAmount(expenseBalanceAmount);
        return expense;
    }

    public List<Expense> initExpense(List<Expense> expenses) {
        for (Expense expense : expenses) {
            expense = initExpense(expense);
        }
        return expenses;
    }

    /*
     QUERY ######################################################################
     */
    public List<Expense> findByDate(Date date1, Date date2) {
        String sql = "SELECT a FROM Expense a WHERE a.expenseDate BETWEEN :date1 AND :date2";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("date1", date1);
        params.put("date2", date2);
        return expenseFacade.findbyQuery(sql, params);
    }

    public List<Expense> findByExpenseMainType(ExpenseMainType expenseMainType) {
        String sql = "SELECT a FROM Expense a WHERE a.expenseSubType.expenseMainType=:expenseMainType ";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expenseMainType", expenseMainType);
        return expenseFacade.findbyQuery(sql, params);
    }

    public List<Expense> findByExpenseSubType(ExpenseSubType expenseSubType) {
        String sql = "SELECT a FROM Expense a WHERE a.expenseSubType=:expenseSubType";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expenseSubType", expenseSubType);
        return expenseFacade.findbyQuery(sql, params);
    }

    public List<Expense> findByType(String expenseType) {
        String sql = "SELECT a FROM Expense a WHERE a.expenseType=:expenseType";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expenseType", expenseType);
        return expenseFacade.findbyQuery(sql, params);
    }

    public List<Expense> findByType(String expenseType, ExpenseMainType expenseMainType) {
        String sql = "SELECT a FROM Expense a WHERE a.expenseType=:expenseType";
        sql += " AND a.expenseSubType.expenseMainType=:expenseMainType ";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expenseType", expenseType);
        params.put("expenseMainType", expenseMainType);
        return expenseFacade.findbyQuery(sql, params);
    }
    
    public List<Expense> findByType(String expenseType, ExpenseSubType expenseSubType) {
        String sql = "SELECT a FROM Expense a WHERE a.expenseType=:expenseType";
        sql += " AND a.expenseSubType=:expenseSubType ";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expenseType", expenseType);
        params.put("expenseSubType", expenseSubType);
        return expenseFacade.findbyQuery(sql, params);
    }
}
