/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.data.model;


import com.cbcit.expensemanager.domains.ExpenseRefund;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Amith Fernando
 */
public class ExpenseRefundDataModel extends ListDataModel<ExpenseRefund> implements SelectableDataModel<ExpenseRefund>, Serializable {

    public ExpenseRefundDataModel() {
    }

    public ExpenseRefundDataModel(List<ExpenseRefund> object) {
        super(object);
    }

    @Override
    public ExpenseRefund getRowData(String rowKey) {
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  

        List<ExpenseRefund> objectList = (List<ExpenseRefund>) getWrappedData();

        for (ExpenseRefund object : objectList) {
            if (object.getId().equals(rowKey)) {
                return object;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(ExpenseRefund object) {
        return object.getId();
    }
}
