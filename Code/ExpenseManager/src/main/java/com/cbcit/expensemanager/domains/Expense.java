/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cbcit.expensemanager.domains;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Amith
 */
@Entity
@Table(name = "expense")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Expense.findAll", query = "SELECT e FROM Expense e"),
    @NamedQuery(name = "Expense.findById", query = "SELECT e FROM Expense e WHERE e.id = :id"),
    @NamedQuery(name = "Expense.findByName", query = "SELECT e FROM Expense e WHERE e.name = :name"),
    @NamedQuery(name = "Expense.findByExpenseNo", query = "SELECT e FROM Expense e WHERE e.expenseNo = :expenseNo"),
    @NamedQuery(name = "Expense.findByExpenseAmount", query = "SELECT e FROM Expense e WHERE e.expenseAmount = :expenseAmount"),
    @NamedQuery(name = "Expense.findByExpenseDate", query = "SELECT e FROM Expense e WHERE e.expenseDate = :expenseDate"),
    @NamedQuery(name = "Expense.findByExpenseType", query = "SELECT e FROM Expense e WHERE e.expenseType = :expenseType")})
public class Expense implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "expense_no")
    private String expenseNo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "expense_amount")
    private double expenseAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "expense_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expenseDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "expense_type")
    private String expenseType;
    @JoinColumn(name = "expense_sub_type", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ExpenseSubType expenseSubType;
    @JoinColumn(name = "recurring_expense", referencedColumnName = "id")
    @ManyToOne
    private RecurringExpense recurringExpense;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "expenseid")
    private List<ExpensePayment> expensePaymentList;
     //TRANSIENT ################################################################
    @Transient
    private double expensePaidAmount;
    @Transient
    private double expenseBalanceAmount;

    public double getExpensePaidAmount() {
        return expensePaidAmount;
    }

    public void setExpensePaidAmount(double expensePaidAmount) {
        this.expensePaidAmount = expensePaidAmount;
    }

    public double getExpenseBalanceAmount() {
        return expenseBalanceAmount;
    }

    public void setExpenseBalanceAmount(double expenseBalanceAmount) {
        this.expenseBalanceAmount = expenseBalanceAmount;
    }

    //TRANSINET ################################################################

    public Expense() {
    }

    public Expense(Integer id) {
        this.id = id;
    }

    public Expense(Integer id, String name, String expenseNo, double expenseAmount, Date expenseDate, String expenseType) {
        this.id = id;
        this.name = name;
        this.expenseNo = expenseNo;
        this.expenseAmount = expenseAmount;
        this.expenseDate = expenseDate;
        this.expenseType = expenseType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExpenseNo() {
        return expenseNo;
    }

    public void setExpenseNo(String expenseNo) {
        this.expenseNo = expenseNo;
    }

    public double getExpenseAmount() {
        return expenseAmount;
    }

    public void setExpenseAmount(double expenseAmount) {
        this.expenseAmount = expenseAmount;
    }

    public Date getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(Date expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public ExpenseSubType getExpenseSubType() {
        return expenseSubType;
    }

    public void setExpenseSubType(ExpenseSubType expenseSubType) {
        this.expenseSubType = expenseSubType;
    }

    public RecurringExpense getRecurringExpense() {
        return recurringExpense;
    }

    public void setRecurringExpense(RecurringExpense recurringExpense) {
        this.recurringExpense = recurringExpense;
    }

    @XmlTransient
    public List<ExpensePayment> getExpensePaymentList() {
        return expensePaymentList;
    }

    public void setExpensePaymentList(List<ExpensePayment> expensePaymentList) {
        this.expensePaymentList = expensePaymentList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Expense)) {
            return false;
        }
        Expense other = (Expense) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cbcit.expensemanager.domains.Expense[ id=" + id + " ]";
    }
    
}
