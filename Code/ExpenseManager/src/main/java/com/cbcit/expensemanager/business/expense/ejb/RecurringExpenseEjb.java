/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cbcit.expensemanager.business.expense.ejb;

import com.amithfernando.web.exception.CreateException;
import com.cbcit.expensemanager.dao.RecurringExpenseFacade;
import com.cbcit.expensemanager.domains.ExpenseSubType;
import com.cbcit.expensemanager.domains.RecurringExpense;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 *
 * @author Amith
 */
@Stateless
public class RecurringExpenseEjb {

    @EJB
    private RecurringExpenseFacade recurringExpenseFacade;
    @EJB
    private ExpenseEjb expenseEjb;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void save(RecurringExpense recurringExpense) throws CreateException {
        recurringExpenseFacade.create(recurringExpense);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void edit(RecurringExpense recurringExpense) throws CreateException {
        recurringExpenseFacade.edit(recurringExpense);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void delete(RecurringExpense recurringExpense) throws CreateException {
        recurringExpenseFacade.remove(recurringExpense);
    }

    public List<RecurringExpense> findAll() {
        return recurringExpenseFacade.findAll();
    }

    public RecurringExpense findByName(String name) {
        String sql = "SELECT a FROM RecurringExpense a WHERE a.name=:name";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", name);
        return recurringExpenseFacade.findbyQuerySingle(sql, params);
    }
    
    public boolean isDuplicateNameOnSave(String name) {
        RecurringExpense object = findByName(name);
        if (object != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isDuplicateNameOnEdit(RecurringExpense newObject) {
        boolean isDuplicate;
        RecurringExpense oldObject = recurringExpenseFacade.find(newObject.getId());
        if (oldObject.getName().equals(newObject.getName())) {
            isDuplicate = false;
        } else {
            isDuplicate = isDuplicateNameOnSave(newObject.getName());
        }
        return isDuplicate;
    }
    
    public List<RecurringExpense> findByExpenseSubType(ExpenseSubType expenseSubType) {
        String sql = "SELECT a FROM RecurringExpense a WHERE a.expenseSubType=:expenseSubType";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expenseSubType", expenseSubType);
        return recurringExpenseFacade.findbyQuery(sql, params);
    }

    public boolean isCanDelete(RecurringExpense recurringExpense) {
       if(!expenseEjb.findByRecurngExpense(recurringExpense).isEmpty()){
           return false;
       }else{
           return true;
       }
    }
}
