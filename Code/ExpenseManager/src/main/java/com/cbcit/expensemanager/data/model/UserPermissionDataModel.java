/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.data.model;


import com.cbcit.expensemanager.domains.UserPermission;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Amith Fernando
 */
public class UserPermissionDataModel extends ListDataModel<UserPermission> implements SelectableDataModel<UserPermission>, Serializable {

    public UserPermissionDataModel() {
    }

    public UserPermissionDataModel(List<UserPermission> object) {
        super(object);
    }

    @Override
    public UserPermission getRowData(String rowKey) {
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  

        List<UserPermission> objectList = (List<UserPermission>) getWrappedData();

        for (UserPermission object : objectList) {
            if (object.getId().equals(rowKey)) {
                return object;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(UserPermission object) {
        return object.getId();
    }
}
