/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.expense.ejb;


import com.amithfernando.web.exception.CreateException;
import com.cbcit.expensemanager.dao.ExpensePaymentFacade;
import com.cbcit.expensemanager.domains.Expense;
import com.cbcit.expensemanager.domains.ExpenseMainType;
import com.cbcit.expensemanager.domains.ExpensePayment;
import com.cbcit.expensemanager.domains.ExpenseSubType;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 *
 * @author Admin
 */
@Stateless
public class ExpensePaymentEjb {

    @EJB
    private ExpensePaymentFacade expensePaymentFacade;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void save(ExpensePayment expensePayment) throws CreateException {
        expensePaymentFacade.create(expensePayment);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void edit(ExpensePayment expensePayment) throws CreateException {
        expensePaymentFacade.edit(expensePayment);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void delete(ExpensePayment expensePayment) throws CreateException {
        expensePaymentFacade.remove(expensePayment);
    }

    public List<ExpensePayment> findAll() {
        return expensePaymentFacade.findAll();
    }

    public ExpensePayment findByName(String name) {
        String sql = "SELECT a FROM ExpensePayment a WHERE a.name=:name";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", name);
        return expensePaymentFacade.findbyQuerySingle(sql, params);
    }
    
    /*
    * SECTION NEW ##############################################################
    */
    
     public List<ExpensePayment> findByExpense(Expense expenseid) {
        String sql = "SELECT a FROM ExpensePayment a WHERE a.expenseid=:expenseid";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expenseid", expenseid);
        return expensePaymentFacade.findbyQuery(sql, params);
    }
     
      public List<ExpensePayment> findByDate(Date date1,Date date2) {
        String sql = "SELECT a FROM ExpensePayment a WHERE a.paymentDate BETWEEN :date1 AND :date2";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("date1", date1);
        params.put("date2", date2);
        return expensePaymentFacade.findbyQuery(sql, params);
    }
      
    

    public List<ExpensePayment> findByExpenseMainType(ExpenseMainType expenseMainType) {
        String sql = "SELECT a FROM ExpensePayment a WHERE a.expenseid.expenseSubType.expenseMainType=:expenseMainType ";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expenseMainType", expenseMainType);
        return expensePaymentFacade.findbyQuery(sql, params);
    }

    public List<ExpensePayment> findByExpenseSubType(ExpenseSubType expenseSubType) {
        String sql = "SELECT a FROM ExpensePayment a WHERE a.expenseid.expenseSubType=:expenseSubType";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expenseSubType", expenseSubType);
        return expensePaymentFacade.findbyQuery(sql, params);
    }

    public List<ExpensePayment> findByType(String expenseType) {
        String sql = "SELECT a FROM ExpensePayment a WHERE a.expenseid.expenseType=:expenseType";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expenseType", expenseType);
        return expensePaymentFacade.findbyQuery(sql, params);
    }

    public List<ExpensePayment> findByType(String expenseType, ExpenseMainType expenseMainType) {
        String sql = "SELECT a FROM ExpensePayment a WHERE a.expenseid.expenseType=:expenseType";
        sql += " AND a.expenseid.expenseSubType.expenseMainType=:expenseMainType ";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expenseType", expenseType);
        params.put("expenseMainType", expenseMainType);
        return expensePaymentFacade.findbyQuery(sql, params);
    }
    
    public List<ExpensePayment> findByType(String expenseType, ExpenseSubType expenseSubType) {
        String sql = "SELECT a FROM ExpensePayment a WHERE a.expenseid.expenseType=:expenseType";
        sql += " AND a.expenseid.expenseSubType=:expenseSubType ";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expenseType", expenseType);
        params.put("expenseSubType", expenseSubType);
        return expensePaymentFacade.findbyQuery(sql, params);
    }



}
