/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cbcit.expensemanager.domains;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Amith
 */
@Entity
@Table(name = "recurring_expense")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RecurringExpense.findAll", query = "SELECT r FROM RecurringExpense r"),
    @NamedQuery(name = "RecurringExpense.findById", query = "SELECT r FROM RecurringExpense r WHERE r.id = :id"),
    @NamedQuery(name = "RecurringExpense.findByName", query = "SELECT r FROM RecurringExpense r WHERE r.name = :name"),
    @NamedQuery(name = "RecurringExpense.findByStartDate", query = "SELECT r FROM RecurringExpense r WHERE r.startDate = :startDate"),
    @NamedQuery(name = "RecurringExpense.findByRecurringDuration", query = "SELECT r FROM RecurringExpense r WHERE r.recurringDuration = :recurringDuration"),
    @NamedQuery(name = "RecurringExpense.findByRecurringType", query = "SELECT r FROM RecurringExpense r WHERE r.recurringType = :recurringType"),
    @NamedQuery(name = "RecurringExpense.findByExpenseAmount", query = "SELECT r FROM RecurringExpense r WHERE r.expenseAmount = :expenseAmount")})
public class RecurringExpense implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "recurring_duration")
    private int recurringDuration;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "recurring_type")
    private String recurringType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "expense_amount")
    private double expenseAmount;
    @OneToMany(mappedBy = "recurringExpense")
    private List<Expense> expenseList;
    @JoinColumn(name = "expense_sub_type", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ExpenseSubType expenseSubType;

    public RecurringExpense() {
    }

    public RecurringExpense(Integer id) {
        this.id = id;
    }

    public RecurringExpense(Integer id, String name, Date startDate, int recurringDuration, String recurringType, double expenseAmount) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.recurringDuration = recurringDuration;
        this.recurringType = recurringType;
        this.expenseAmount = expenseAmount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getRecurringDuration() {
        return recurringDuration;
    }

    public void setRecurringDuration(int recurringDuration) {
        this.recurringDuration = recurringDuration;
    }

    public String getRecurringType() {
        return recurringType;
    }

    public void setRecurringType(String recurringType) {
        this.recurringType = recurringType;
    }

    public double getExpenseAmount() {
        return expenseAmount;
    }

    public void setExpenseAmount(double expenseAmount) {
        this.expenseAmount = expenseAmount;
    }

    @XmlTransient
    public List<Expense> getExpenseList() {
        return expenseList;
    }

    public void setExpenseList(List<Expense> expenseList) {
        this.expenseList = expenseList;
    }

    public ExpenseSubType getExpenseSubType() {
        return expenseSubType;
    }

    public void setExpenseSubType(ExpenseSubType expenseSubType) {
        this.expenseSubType = expenseSubType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RecurringExpense)) {
            return false;
        }
        RecurringExpense other = (RecurringExpense) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cbcit.expensemanager.domains.RecurringExpense[ id=" + id + " ]";
    }
    
}
