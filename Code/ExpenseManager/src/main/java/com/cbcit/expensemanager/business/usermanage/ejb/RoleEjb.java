/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.usermanage.ejb;


import com.cbcit.expensemanager.dao.PermissionFacade;
import com.cbcit.expensemanager.dao.RoleFacade;
import com.cbcit.expensemanager.domains.Permission;
import com.cbcit.expensemanager.domains.Role;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Amith Fernando
 */
@Stateless
public class RoleEjb {

    @EJB
    private RoleFacade roleFacade;
    @EJB
    private PermissionFacade permissionFacade;

    public List<Role> findAll() {
        return roleFacade.findAll();
    }

    public List<Permission> getPermissions(Role role) {
        String sql = "SELECT a FROM Permission a WHERE a.role = :role";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("role", role);
        return permissionFacade.findbyQuery(sql, params);
    }
    public Role getFromName(String name) {
        String sql = "SELECT a FROM Role a WHERE a.name = :name";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", name);
        return roleFacade.findbyQuery(sql, params).get(0);
    }
    public List getAll(String sql,Map<String,Object> params) {
        return roleFacade.findbyQuery(sql, params);
    }
}
