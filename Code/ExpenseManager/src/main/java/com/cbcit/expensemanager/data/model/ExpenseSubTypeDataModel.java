/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.data.model;


import com.cbcit.expensemanager.domains.ExpenseSubType;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Amith Fernando
 */
public class ExpenseSubTypeDataModel extends ListDataModel<ExpenseSubType> implements SelectableDataModel<ExpenseSubType>, Serializable {

    public ExpenseSubTypeDataModel() {
    }

    public ExpenseSubTypeDataModel(List<ExpenseSubType> object) {
        super(object);
    }

    @Override
    public ExpenseSubType getRowData(String rowKey) {
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  

        List<ExpenseSubType> objectList = (List<ExpenseSubType>) getWrappedData();

        for (ExpenseSubType object : objectList) {
            if (object.getId().equals(rowKey)) {
                return object;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(ExpenseSubType object) {
        return object.getId();
    }
}
