/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.data.model;


import com.cbcit.expensemanager.domains.User;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Amith Fernando
 */
public class UserDataModel extends ListDataModel<User> implements SelectableDataModel<User>,Serializable{
    
     public UserDataModel() {  
    }  
  
    public UserDataModel(List<User> object) {  
        super(object);  
    }  
      
    @Override  
    public User getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
          
        List<User> objectList = (List<User>) getWrappedData();  
          
        for(User object : objectList) {  
            if(object.getId().equals(rowKey))  
                return object;  
        }  
          
        return null;  
    }  
  
    @Override
    public Object getRowKey(User object) {  
        return object.getId();
    } 
}
