/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.data.model;


import com.cbcit.expensemanager.domains.RecurringExpense;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Amith Fernando
 */
public class RecurringExpenseDataModel extends ListDataModel<RecurringExpense> implements SelectableDataModel<RecurringExpense>, Serializable {

    public RecurringExpenseDataModel() {
    }

    public RecurringExpenseDataModel(List<RecurringExpense> object) {
        super(object);
    }

    @Override
    public RecurringExpense getRowData(String rowKey) {
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  

        List<RecurringExpense> objectList = (List<RecurringExpense>) getWrappedData();

        for (RecurringExpense object : objectList) {
            if (object.getId().equals(rowKey)) {
                return object;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(RecurringExpense object) {
        return object.getId();
    }
}
