/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cbcit.expensemanager.domains;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amith
 */
@Entity
@Table(name = "expense_refund")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExpenseRefund.findAll", query = "SELECT e FROM ExpenseRefund e"),
    @NamedQuery(name = "ExpenseRefund.findById", query = "SELECT e FROM ExpenseRefund e WHERE e.id = :id"),
    @NamedQuery(name = "ExpenseRefund.findByRefundDate", query = "SELECT e FROM ExpenseRefund e WHERE e.refundDate = :refundDate"),
    @NamedQuery(name = "ExpenseRefund.findByAmount", query = "SELECT e FROM ExpenseRefund e WHERE e.amount = :amount")})
public class ExpenseRefund implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "refund_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date refundDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private double amount;
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
    @JoinColumn(name = "expense_payment", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ExpensePayment expensePayment;

    public ExpenseRefund() {
    }

    public ExpenseRefund(Integer id) {
        this.id = id;
    }

    public ExpenseRefund(Integer id, Date refundDate, double amount) {
        this.id = id;
        this.refundDate = refundDate;
        this.amount = amount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getRefundDate() {
        return refundDate;
    }

    public void setRefundDate(Date refundDate) {
        this.refundDate = refundDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ExpensePayment getExpensePayment() {
        return expensePayment;
    }

    public void setExpensePayment(ExpensePayment expensePayment) {
        this.expensePayment = expensePayment;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExpenseRefund)) {
            return false;
        }
        ExpenseRefund other = (ExpenseRefund) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cbcit.expensemanager.domains.ExpenseRefund[ id=" + id + " ]";
    }
    
}
