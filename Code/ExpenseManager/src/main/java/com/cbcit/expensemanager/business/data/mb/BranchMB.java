/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.data.mb;

import com.amithfernando.web.mb.AbstractSecurityCheckMB;
import com.amithfernando.web.mb.CrudManagedBean;
import static com.amithfernando.web.mb.CrudManagedBean.DUPLICATE_RECORD;
import static com.amithfernando.web.mb.CrudManagedBean.NO_PERMISSION;
import static com.amithfernando.web.mb.CrudManagedBean.PLEASE_SELECT_RECORD;
import com.amithfernando.web.util.JsfUtil;
import com.cbcit.expensemanager.business.data.ejb.BranchEjb;
import com.cbcit.expensemanager.data.model.BranchDataModel;
import com.cbcit.expensemanager.domains.Branch;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Admin
 */
@ManagedBean
@ViewScoped
public class BranchMB extends AbstractSecurityCheckMB implements CrudManagedBean, Serializable {

    private Branch branch;
    private List<Branch> branchList;
    private BranchDataModel branchDataModel;
    @EJB
    private BranchEjb branchEjb;
    //
    private String validateMsg;

    @PostConstruct
    public void init() {
        newAction();
        searchAction();
        //
        initSecurityCheck();
    }

    @Override
    public void newAction() {
        branch = new Branch();
    }

    @Override
    public void saveAction() {
        try {
            if (branch.getId() == null) {
                if (isValidOnSave()) {
                    branchEjb.save(branch);
                    JsfUtil.addSuccessMessage(SAVE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                if (isVaildOnEdit()) {
                    branchEjb.edit(branch);
                    JsfUtil.addSuccessMessage(UPDATE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            }
            searchAction();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValidOnSave() {
        if (!currentUser.isPermitted("branch:create")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (branchEjb.isDuplicateNameOnSave(branch.getName())) {
            validateMsg = DUPLICATE_RECORD;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isVaildOnEdit() {
        if (!currentUser.isPermitted("branch:edit")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else if (branchEjb.isDuplicateNameOnEdit(branch)) {
            validateMsg = DUPLICATE_RECORD;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void deleteAction() {
        try {
            if (branch != null) {
                if (isCanDelete()) {
                    branchEjb.delete(branch);
                    JsfUtil.addSuccessMessage(DELETE_MSG);
                    newAction();
                    searchAction();
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
            e.printStackTrace();
        }
    }

    @Override
    public void onDeleteBtnClick() {
        RequestContext context = RequestContext.getCurrentInstance();
        if (branch.getId() != null) {
            context.execute("dlgDeleteConfirm.show()");
        } else {
            context.update("frmData:msg");
            JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
        }
    }

    @Override
    public boolean isCanDelete() {
        if (!currentUser.isPermitted("branch:delete")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void searchAction() {
        branchList = branchEjb.findAll();
        branchDataModel = new BranchDataModel(branchList);
    }

    @Override
    public void onRecordSelect(SelectEvent evt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /*
     * GETTER & SETTER #########################################################
     */
    public BranchDataModel getBranchDataModel() {
        return branchDataModel;
    }

    public void setBranchDataModel(BranchDataModel branchDataModel) {
        this.branchDataModel = branchDataModel;
    }

    public BranchEjb getBranchEjb() {
        return branchEjb;
    }

    public void setBranchEjb(BranchEjb branchEjb) {
        this.branchEjb = branchEjb;
    }

    public String getValidateMsg() {
        return validateMsg;
    }

    public void setValidateMsg(String validateMsg) {
        this.validateMsg = validateMsg;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public List<Branch> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<Branch> branchList) {
        this.branchList = branchList;
    }
}
