/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.expense.ejb;

import com.amithfernando.web.util.DateUtil;
import com.cbcit.expensemanager.domains.Expense;
import com.cbcit.expensemanager.domains.RecurringExpense;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;

/**
 *
 * @author Amith
 */
@Stateless
public class RecuringExpenseServiceEjb {

    @EJB
    private RecurringExpenseEjb recurringExpenseEjb;
    @EJB
    private ExpenseEjb expenseEjb;

    @Schedule(minute ="*/2", hour = "*")
    public void runEveryMinute() {
        addExpense();
    }

    private void addExpense() {
        List<RecurringExpense> recurringExpenses = recurringExpenseEjb.findAll();
        Date currnetDate = new Date();
        for (RecurringExpense recurringExpense : recurringExpenses) {
            Date startDate = recurringExpense.getStartDate();
            String type = recurringExpense.getRecurringType();
            int cycle = recurringExpense.getRecurringDuration();
            int durationDays = 0;
            double modulas = 0;
            if (type.equals("Daily")) {
                durationDays = DateUtil.dateDiffrence(startDate, currnetDate, "DAYS");
            } else if (type.equals("Monthly")) {
                durationDays = DateUtil.dateDiffrence(startDate, currnetDate, "MONTH");
            } else if (type.equals("Yearly")) {
                durationDays = DateUtil.dateDiffrence(startDate, currnetDate, "YEARS");
            }
            modulas = durationDays % cycle;
            if (modulas == 0) {
                //add expense
                insertExpense(recurringExpense, currnetDate);
            }

        }
    }

    private void insertExpense(RecurringExpense recurringExpense, Date date) {
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        List<Expense> expenses = expenseEjb.findByRecurngExpenseAndDate(recurringExpense, date);
        if (expenses.isEmpty()) {
            System.out.println("####### -> Adding Recuring Expense for recurring "+recurringExpense.getId()+" date "+date.toString());
            Expense expense = new Expense();
            expense.setExpenseAmount(recurringExpense.getExpenseAmount());
            expense.setExpenseDate(date);
            expense.setExpenseSubType(recurringExpense.getExpenseSubType());
            expense.setRecurringExpense(recurringExpense);
            expense.setName(recurringExpense.getName());
            expense.setExpenseType("Recurring");
            expenseEjb.save(expense);
        }
    }

}
