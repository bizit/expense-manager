/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.expense.mb;

import com.amithfernando.web.mb.AbstractSecurityCheckMB;
import com.amithfernando.web.util.JasperReportUtil;
import com.amithfernando.web.util.JsfUtil;
import com.cbcit.expensemanager.business.data.ejb.ExpenseSubTypeEjb;
import com.cbcit.expensemanager.business.expense.ejb.ExpensePaymentEjb;
import com.cbcit.expensemanager.domains.ExpenseMainType;
import com.cbcit.expensemanager.domains.ExpensePayment;
import com.cbcit.expensemanager.domains.ExpenseSubType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author Amith
 */
@ManagedBean
@ViewScoped
public class PaymentSummaryMB extends AbstractSecurityCheckMB implements Serializable {

    private Date date1, date2;
    private ExpenseMainType expenseMainType;
    private List<ExpenseSubType> expenseSubTypeList;
    private ExpenseSubType selectedExpenseSubType;
    private String expenseType = "All";
    private String searchBy = "Date";
    private List<ExpensePayment> expensePaymentList;
    @EJB
    private ExpensePaymentEjb expensePaymentEjb;
    @EJB
    private ExpenseSubTypeEjb expenseSubTypeEjb;
    //
    private boolean isDate;
    private boolean isMainType;
    private boolean isSubType;
    private boolean isType;
    //
        private double totalRecords,totalAmount,totalPaidAmount;

    @PostConstruct
    private void init() {
        initSecurityCheck();
        expensePaymentList = new ArrayList<ExpensePayment>();
        onSearchBySelect();
    }

    public void onSearchBySelect() {
        isDate = isType = isMainType = isSubType = false;
        if (searchBy.equals("Date")) {
            isDate = true;
        } else if (searchBy.equals("MainType")) {
            isMainType = true;
        } else if (searchBy.equals("SubType")) {
            isSubType = isMainType = true;
        } else if (searchBy.equals("Type")) {
            isType = true;
        } else if (searchBy.equals("TypeAndMainType")) {
            isType = isMainType = true;
        } else if (searchBy.equals("TypeAndSubType")) {
            isType = isMainType = isSubType = true;
        }
    }

    public void searchAction() {
        if (searchBy.equals("Date")) {
            expensePaymentList = expensePaymentEjb.findByDate(date1, date2);
        } else if (searchBy.equals("MainType")) {
            expensePaymentList = expensePaymentEjb.findByExpenseMainType(expenseMainType);
        } else if (searchBy.equals("SubType")) {
            expensePaymentList = expensePaymentEjb.findByExpenseSubType(selectedExpenseSubType);
        } else if (searchBy.equals("Type")) {
            expensePaymentList = expensePaymentEjb.findByType(expenseType);
        } else if (searchBy.equals("TypeAndMainType")) {
            expensePaymentList = expensePaymentEjb.findByType(expenseType, expenseMainType);
        } else if (searchBy.equals("TypeAndSubType")) {
            expensePaymentList = expensePaymentEjb.findByType(expenseType, selectedExpenseSubType);
        }else{
            expensePaymentList = expensePaymentEjb.findAll();
        }
        calculateTotals();
    }

    public void reportAction() {
        try {
            String reportPath = JsfUtil.getAbsolutePath("/resources/reports/ExpensePaymentSummery.jasper");
            String subReportPath = JsfUtil.getAbsolutePath("/resources/reports");
            String realPath = JsfUtil.getAbsolutePath("");
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("realPath", realPath);
            params.put("SUBREPORT_DIR", subReportPath);
            JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(expensePaymentList);
            JasperReportUtil.toPDF(beanCollectionDataSource, reportPath, params, "Inquiry Summary");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void onExpenseMainTypeSelect() {
        expenseSubTypeList = expenseSubTypeEjb.findByExpenseMainType(expenseMainType);
    }
    
      
    public void calculateTotals(){
        totalRecords=totalAmount=totalPaidAmount=0;
        for (ExpensePayment expensePayment : expensePaymentList) {
            totalRecords++;
            totalAmount+=expensePayment.getExpenseid().getExpenseAmount();
            totalPaidAmount+=expensePayment.getAmount();
        }
    }

    /*
    GETTER & SETTER ############################################################
     */
    public double getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(double totalRecords) {
        this.totalRecords = totalRecords;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(double totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }
    
    
    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    public ExpenseMainType getExpenseMainType() {
        return expenseMainType;
    }

    public void setExpenseMainType(ExpenseMainType expenseMainType) {
        this.expenseMainType = expenseMainType;
    }

    public List<ExpenseSubType> getExpenseSubTypeList() {
        return expenseSubTypeList;
    }

    public void setExpenseSubTypeList(List<ExpenseSubType> expenseSubTypeList) {
        this.expenseSubTypeList = expenseSubTypeList;
    }

    public ExpenseSubType getSelectedExpenseSubType() {
        return selectedExpenseSubType;
    }

    public void setSelectedExpenseSubType(ExpenseSubType selectedExpenseSubType) {
        this.selectedExpenseSubType = selectedExpenseSubType;
    }

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    
    public String getSearchBy() {
        return searchBy;
    }

    public void setSearchBy(String searchBy) {
        this.searchBy = searchBy;
    }

    public List<ExpensePayment> getExpensePaymentList() {
        return expensePaymentList;
    }

    public void setExpensePaymentList(List<ExpensePayment> expensePaymentList) {
        this.expensePaymentList = expensePaymentList;
    }

    public ExpensePaymentEjb getExpensePaymentEjb() {
        return expensePaymentEjb;
    }

    public void setExpensePaymentEjb(ExpensePaymentEjb expensePaymentEjb) {
        this.expensePaymentEjb = expensePaymentEjb;
    }

    public ExpenseSubTypeEjb getExpenseSubTypeEjb() {
        return expenseSubTypeEjb;
    }

    public void setExpenseSubTypeEjb(ExpenseSubTypeEjb expenseSubTypeEjb) {
        this.expenseSubTypeEjb = expenseSubTypeEjb;
    }

    public boolean isIsDate() {
        return isDate;
    }

    public void setIsDate(boolean isDate) {
        this.isDate = isDate;
    }

    public boolean isIsMainType() {
        return isMainType;
    }

    public void setIsMainType(boolean isMainType) {
        this.isMainType = isMainType;
    }

    public boolean isIsSubType() {
        return isSubType;
    }

    public void setIsSubType(boolean isSubType) {
        this.isSubType = isSubType;
    }

    public boolean isIsType() {
        return isType;
    }

    public void setIsType(boolean isType) {
        this.isType = isType;
    }
   
}
