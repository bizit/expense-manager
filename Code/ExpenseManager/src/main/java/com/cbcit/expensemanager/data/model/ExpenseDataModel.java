/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.data.model;


import com.cbcit.expensemanager.domains.Expense;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Amith Fernando
 */
public class ExpenseDataModel extends ListDataModel<Expense> implements SelectableDataModel<Expense>, Serializable {

    public ExpenseDataModel() {
    }

    public ExpenseDataModel(List<Expense> object) {
        super(object);
    }

    @Override
    public Expense getRowData(String rowKey) {
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  

        List<Expense> objectList = (List<Expense>) getWrappedData();

        for (Expense object : objectList) {
            if (object.getId().equals(rowKey)) {
                return object;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(Expense object) {
        return object.getId();
    }
}
