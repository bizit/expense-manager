/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.data.model;


import com.cbcit.expensemanager.domains.Role;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Amith Fernando
 */
public class RoleDataModel extends ListDataModel<Role> implements SelectableDataModel<Role>, Serializable {

    public RoleDataModel() {
    }

    public RoleDataModel(List<Role> object) {
        super(object);
    }

    @Override
    public Role getRowData(String rowKey) {
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  

        List<Role> objectList = (List<Role>) getWrappedData();

        for (Role object : objectList) {
            if (object.getId().equals(rowKey)) {
                return object;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(Role object) {
        return object.getId();
    }
}
