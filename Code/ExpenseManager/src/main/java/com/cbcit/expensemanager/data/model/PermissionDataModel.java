/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.data.model;


import com.cbcit.expensemanager.domains.Permission;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Amith Fernando
 */
public class PermissionDataModel extends ListDataModel<Permission> implements SelectableDataModel<Permission>, Serializable {

    public PermissionDataModel() {
    }

    public PermissionDataModel(List<Permission> object) {
        super(object);
    }

    @Override
    public Permission getRowData(String rowKey) {
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  

        List<Permission> objectList = (List<Permission>) getWrappedData();

        for (Permission object : objectList) {
            if (object.getId().equals(rowKey)) {
                return object;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(Permission object) {
        return object.getId();
    }
}
