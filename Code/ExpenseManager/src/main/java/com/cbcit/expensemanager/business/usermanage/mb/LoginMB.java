/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.usermanage.mb;

import com.amithfernando.web.util.JsfUtil;
import com.cbcit.expensemanager.business.expense.ejb.ExpenseEjb;
import com.cbcit.expensemanager.business.usermanage.ejb.UserEjb;
import com.cbcit.expensemanager.domains.Expense;
import com.cbcit.expensemanager.domains.User;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

/**
 *
 * @author Amith
 */
@ManagedBean
@RequestScoped
public class LoginMB {

    private String userName;
    private String password;
    private boolean rememberMe;
    public static final String DATA_ENTRY = "pages/dataentry/dataentryDashboard.xhtml";
    public static final String ADMIN_ENTRY = "pages/admin/adminDashboard.xhtml";
    public static final String CUSTOMER_ENTRY = "pages/customer/customerDashboard.xhtml";
    //
    @EJB
    private UserEjb userEjb;

    public void login() {
        try {
            if (!isTrialExpired()) {
                SecurityUtils.getSubject().login(new UsernamePasswordToken(userName, password, rememberMe));

                if (SecurityUtils.getSubject().hasRole("DataEntry")) {
                    FacesContext.getCurrentInstance().getExternalContext().redirect(DATA_ENTRY);
                } else if (SecurityUtils.getSubject().hasRole("Admin")) {
                    FacesContext.getCurrentInstance().getExternalContext().redirect(ADMIN_ENTRY);
                } else if (SecurityUtils.getSubject().hasRole("Customer")) {
                    FacesContext.getCurrentInstance().getExternalContext().redirect(CUSTOMER_ENTRY);
                }
                //
                Subject currentUser = SecurityUtils.getSubject();
                Session session = currentUser.getSession();
                session.setAttribute("user", userName);
                //
                User user = userEjb.findByUserName(userName);
                userEjb.setUserLastlogin(user);
            } else {
                throw new LockedAccountException("Trial preiod expired!");
            }
            //
        } catch (UnknownAccountException uae) {
            JsfUtil.addErrorMessage("User name incorrect! or not active!");
        } catch (IncorrectCredentialsException ice) {
            JsfUtil.addErrorMessage("Password incorrect!");
        } catch (LockedAccountException lae) {
        } catch (ExcessiveAttemptsException eae) {
        } catch (AuthenticationException ae) {
            JsfUtil.ensureAddErrorMessage(ae, "Error");
        } catch (Exception ae) {
            JsfUtil.ensureAddErrorMessage(ae, "Error");
        }
    }

    public void logout() throws IOException {
        try {
            SecurityUtils.getSubject().logout();
            FacesContext.getCurrentInstance().getExternalContext().redirect("../../login.xhtml");
        } catch (AuthenticationException e) {
            System.out.println("Errro");
            JsfUtil.addErrorMessage("Error " + e);
        }
    }

    public String getLoggedUserName() {
        return JsfUtil.getSessionAttrtibute("user").toString();
    }

    /*
     * TRIAL CHECK #############################################################
     */
    @EJB
    private ExpenseEjb expenseEjb;

    private boolean isTrialExpired() {
        boolean status = false;
        List<Expense> loansList = expenseEjb.findAll();
        if (loansList.size() > 101) {
            status = true;
        }
        return status;
    }

    /*
     * GETTER AND SETTER #######################################################
     */
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }
}
