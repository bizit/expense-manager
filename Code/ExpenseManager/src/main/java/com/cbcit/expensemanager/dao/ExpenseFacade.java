/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cbcit.expensemanager.dao;

import com.cbcit.expensemanager.domains.Expense;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Amith
 */
@Stateless
public class ExpenseFacade extends AbstractFacade<Expense> {
    @PersistenceContext(unitName = "com.cbcit_JobManagementBase_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ExpenseFacade() {
        super(Expense.class);
    }
    
}
