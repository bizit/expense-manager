/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cbcit.expensemanager.application.mb;


import com.cbcit.expensemanager.dao.ExpenseMainTypeFacade;
import com.cbcit.expensemanager.dao.RecurringExpenseFacade;
import com.cbcit.expensemanager.domains.ExpenseMainType;
import com.cbcit.expensemanager.domains.RecurringExpense;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Amith
 */
@ManagedBean
@ApplicationScoped
public class ApplicationMB implements Serializable{
    
    //KASUN AREA ###############################################################
    private List<ExpenseMainType> expenseMainTypeList=new ArrayList<ExpenseMainType>();
    @EJB
    private ExpenseMainTypeFacade expenseMainTypeFacade;
    
  
    
    private List<RecurringExpense> recurringExpenseList=new ArrayList<RecurringExpense>();
    @EJB
    private RecurringExpenseFacade  recurringExpenseFacade;
    
  
    

    /*
    GETTERS AND SETTERS ##############################
     */
    public List<ExpenseMainType> getExpenseMainTypeList() {
        expenseMainTypeList=expenseMainTypeFacade.findAll();
        return expenseMainTypeList;
    }

    public void setExpenseMainTypeList(List<ExpenseMainType> expenseMainTypeList) {
        this.expenseMainTypeList = expenseMainTypeList;
    }
    


    public List<RecurringExpense> getRecurringExpenseList() {
        recurringExpenseList=recurringExpenseFacade.findAll();
        return recurringExpenseList;
    }

    public void setRecurringExpenseList(List<RecurringExpense> recurringExpenseList) {
        this.recurringExpenseList = recurringExpenseList;
    }


    
}
