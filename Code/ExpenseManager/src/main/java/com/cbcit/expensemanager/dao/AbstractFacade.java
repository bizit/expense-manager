/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.dao;

import java.util.List;
import java.util.Map;
import javax.persistence.Cache;
import javax.persistence.EntityManager;

/**
 *
 * @author Amith Fernando
 */
public abstract class AbstractFacade<T> {
     private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public T refresh(T entity) {
        getEntityManager().refresh(entity);
        return entity;
    }

    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0]);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    public List<T> findNamedQuery(String query, Map<String, Object> params) {
        javax.persistence.Query q = getEntityManager().createNamedQuery(query);
        for (Map.Entry<String, Object> en : params.entrySet()) {
            String object = en.getKey();
            Object value = en.getValue();
            q.setParameter(object, value);
        }
        return q.getResultList();
    }

    public List<T> findNamedQueryLimit(String query, Map<String, Object> params, int limit) {
        javax.persistence.Query q = getEntityManager().createNamedQuery(query);
        for (Map.Entry<String, Object> en : params.entrySet()) {
            String object = en.getKey();
            Object value = en.getValue();
            q.setParameter(object, value);
        }
        q.setMaxResults(limit);
        return q.getResultList();
    }

    public List<T> findbyQueryLimit(String query, Map<String, Object> params, int limit) {
        javax.persistence.Query q = getEntityManager().createQuery(query);
        for (Map.Entry<String, Object> en : params.entrySet()) {
            String object = en.getKey();
            Object value = en.getValue();
            q.setParameter(object, value);
        }
        q.setMaxResults(limit);
        return q.getResultList();
    }

    public List<T> findbyQuery(String query, Map<String, Object> params) {
        javax.persistence.Query q = getEntityManager().createQuery(query);
        for (Map.Entry<String, Object> en : params.entrySet()) {
            String object = en.getKey();
            Object value = en.getValue();
            q.setParameter(object, value);
        }
        return q.getResultList();
    }

    public T findbyQuerySingle(String query, Map<String, Object> params) {
        javax.persistence.Query q = getEntityManager().createQuery(query);
        for (Map.Entry<String, Object> en : params.entrySet()) {
            String object = en.getKey();
            Object value = en.getValue();
            q.setParameter(object, value);
        }
        try {
            return (T) q.getSingleResult();
        } catch (javax.persistence.NoResultException e) {
            return null;
        }
    }

    public T findSingleNamedQuery(String query, Map<String, Object> params) {
        try {
            javax.persistence.Query q = getEntityManager().createNamedQuery(query);
            for (Map.Entry<String, Object> en : params.entrySet()) {
                String object = en.getKey();
                Object value = en.getValue();
                q.setParameter(object, value);
            }
            return (T) q.getSingleResult();
        } catch (javax.persistence.NoResultException e) {
            return null;
        }
    }

    public Cache getCache() {
        Cache cache = getEntityManager().getEntityManagerFactory().getCache();
        return null;
    }
    
}
