/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.data.converter;

import com.cbcit.expensemanager.dao.UserFacade;
import com.cbcit.expensemanager.domains.User;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Amith Fernando
 */
@ManagedBean
@FacesConverter(value = "userConverter")
public class UserConverter implements Converter {

    @EJB
    private UserFacade facade;

    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            if (value == null || value.isEmpty()) {
                return null;
            }
            if (!value.matches("\\d+")) {
                throw new ConverterException("The value is not a valid ID number: " + value);
            }
            return facade.find(Integer.valueOf(value));
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value != null && value instanceof User) {
            User object = (User) value;
            return "" + object.getId();
        }
        return "";
    }
}
