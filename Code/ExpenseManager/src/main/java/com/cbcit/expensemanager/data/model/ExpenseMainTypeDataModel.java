/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.data.model;


import com.cbcit.expensemanager.domains.ExpenseMainType;
import java.io.Serializable;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author Amith Fernando
 */
public class ExpenseMainTypeDataModel extends ListDataModel<ExpenseMainType> implements SelectableDataModel<ExpenseMainType>, Serializable {

    public ExpenseMainTypeDataModel() {
    }

    public ExpenseMainTypeDataModel(List<ExpenseMainType> object) {
        super(object);
    }

    @Override
    public ExpenseMainType getRowData(String rowKey) {
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  

        List<ExpenseMainType> objectList = (List<ExpenseMainType>) getWrappedData();

        for (ExpenseMainType object : objectList) {
            if (object.getId().equals(rowKey)) {
                return object;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(ExpenseMainType object) {
        return object.getId();
    }
}
