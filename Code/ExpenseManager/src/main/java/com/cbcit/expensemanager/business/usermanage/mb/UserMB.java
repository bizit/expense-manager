/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.usermanage.mb;



import com.amithfernando.web.mb.AbstractSecurityCheckMB;
import com.amithfernando.web.mb.CrudManagedBean;
import static com.amithfernando.web.mb.CrudManagedBean.DELETE_MSG;
import static com.amithfernando.web.mb.CrudManagedBean.NO_PERMISSION;
import static com.amithfernando.web.mb.CrudManagedBean.PLEASE_SELECT_RECORD;
import static com.amithfernando.web.mb.CrudManagedBean.SAVE_MSG;
import static com.amithfernando.web.mb.CrudManagedBean.UPDATE_MSG;
import com.amithfernando.web.util.JsfUtil;
import com.amithfernando.web.util.PaswordEncription;
import com.cbcit.expensemanager.business.usermanage.ejb.RoleEjb;
import com.cbcit.expensemanager.business.usermanage.ejb.UserEjb;
import com.cbcit.expensemanager.data.model.UserDataModel;
import com.cbcit.expensemanager.domains.Permission;
import com.cbcit.expensemanager.domains.Role;
import com.cbcit.expensemanager.domains.User;
import com.cbcit.expensemanager.domains.UserPermission;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.SelectEvent;


/**
 *
 * @author Amith Fernando
 */
@ManagedBean
@ViewScoped
public class UserMB extends AbstractSecurityCheckMB implements CrudManagedBean, Serializable {

    private User user;
    private List<User> userList;
    private UserDataModel userDataModel;
    @EJB
    private UserEjb userEjb;
    //
    private String validateMsg;
    //
    private final String DEFAULT_PASSWORD = "123456";
    //
    @EJB
    private RoleEjb roleEjb;
    private List<Role> roleList;
    //
    private List<Permission> permissionList;
    private List<Permission> selectedPermissionList;
    //
    private String newPassowrd;
    private String confirmPassword;

    @PostConstruct
    public void init() {
        newAction();
        searchAction();
        initRoles();
        //
        initSecurityCheck();
    }

    @Override
    public void newAction() {
        user = new User();
        user.setCreatedDate(new Date());
        user.setPassword(PaswordEncription.encript(DEFAULT_PASSWORD));
        //
        permissionList = new ArrayList<Permission>();
        selectedPermissionList = new ArrayList<Permission>();
    }

    @Override
    public void saveAction() {

        try {
            if (user.getId() == null) {
                if (isValidOnSave()) {
                    userEjb.save(user, selectedPermissionList);
                    JsfUtil.addSuccessMessage(SAVE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                if (isVaildOnEdit()) {
                    userEjb.edit(user, selectedPermissionList);
                    JsfUtil.addSuccessMessage(UPDATE_MSG);
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            }
            searchAction();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
        }

    }

    @Override
    public boolean isValidOnSave() {
        if (!currentUser.isPermitted("user:create")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isVaildOnEdit() {
        if (!currentUser.isPermitted("user:edit")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void deleteAction() {
        try {
            if (user != null) {
                if (isCanDelete()) {
                    userEjb.delete(user);
                    JsfUtil.addSuccessMessage(DELETE_MSG);
                    newAction();
                    searchAction();
                } else {
                    JsfUtil.addErrorMessage(validateMsg);
                }
            } else {
                JsfUtil.addErrorMessage(PLEASE_SELECT_RECORD);
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
            e.printStackTrace();
        }
    }
    
       @Override
    public void onDeleteBtnClick() {

    }

    @Override
    public boolean isCanDelete() {
        if (!currentUser.isPermitted("user:edit")) {
            validateMsg = NO_PERMISSION;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void searchAction() {
        userList = userEjb.findAll();
        userDataModel = new UserDataModel(userList);
    }

    @Override
    public void onRecordSelect(SelectEvent evt) {
        permissionList = roleEjb.getPermissions(user.getRole());
        //
        List<UserPermission> userPermissions = userEjb.findByUser(user);
        selectedPermissionList = new ArrayList<Permission>();
        for (UserPermission userPermission : userPermissions) {
            selectedPermissionList.add(userPermission.getPermission());
        }
    }

    /*
     * OTHER SECTION ###########################################################
     */
    public void resetPassword() {
        try {
            if (user.getId() != null) {
                user.setPassword(PaswordEncription.encript(DEFAULT_PASSWORD));
                userEjb.chnagePassword(user);
                JsfUtil.addSuccessMessage("Changed Succeded!");
            } else {
                JsfUtil.addErrorMessage("Plsease select user");
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
        }
    }

    private void initRoles() {
        roleList = roleEjb.findAll();
    }

    public void onRoleChange() {
        Role r = user.getRole();
        System.out.println("RRR " + r.getName());
        if (r != null) {
            permissionList = roleEjb.getPermissions(r);
        }

    }

    public void changePassword() {
        try {
            if (newPassowrd.equals(confirmPassword)) {
                String userName = getLoggedUserName();
                System.out.println("User " + userName);
                User u = userEjb.findByUserName(userName);
                u.setPassword(PaswordEncription.encript(newPassowrd));
                userEjb.chnagePassword(u);
                JsfUtil.addSuccessMessage("Changed Succeded!");
            } else {
                JsfUtil.addErrorMessage("Passwords not match");
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error " + e);
        }
    }

    /*
     * GETTER & SETTER #########################################################
     */
    public String getNewPassowrd() {
        return newPassowrd;
    }

    public void setNewPassowrd(String newPassowrd) {
        this.newPassowrd = newPassowrd;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public RoleEjb getRoleEjb() {
        return roleEjb;
    }

    public void setRoleEjb(RoleEjb roleEjb) {
        this.roleEjb = roleEjb;
    }

    public List<Permission> getPermissionList() {
        return permissionList;
    }

    public void setPermissionList(List<Permission> permissionList) {
        this.permissionList = permissionList;
    }

    public List<Permission> getSelectedPermissionList() {
        return selectedPermissionList;
    }

    public void setSelectedPermissionList(List<Permission> selectedPermissionList) {
        this.selectedPermissionList = selectedPermissionList;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    public UserDataModel getUserDataModel() {
        return userDataModel;
    }

    public void setUserDataModel(UserDataModel userDataModel) {
        this.userDataModel = userDataModel;
    }

    public UserEjb getUserEjb() {
        return userEjb;
    }

    public void setUserEjb(UserEjb userEjb) {
        this.userEjb = userEjb;
    }

    public String getValidateMsg() {
        return validateMsg;
    }

    public void setValidateMsg(String validateMsg) {
        this.validateMsg = validateMsg;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
