/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cbcit.expensemanager.business.expense.ejb;


import com.amithfernando.web.exception.CreateException;
import com.cbcit.expensemanager.dao.ExpenseRefundFacade;
import com.cbcit.expensemanager.domains.ExpensePayment;
import com.cbcit.expensemanager.domains.ExpenseRefund;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 *
 * @author Admin
 */
@Stateless
public class ExpenseRefundEjb {

    @EJB
    private ExpenseRefundFacade expenseRefundFacade;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void save(ExpenseRefund expenseRefund) throws CreateException {
        expenseRefundFacade.create(expenseRefund);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void edit(ExpenseRefund expenseRefund) throws CreateException {
        expenseRefundFacade.edit(expenseRefund);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void delete(ExpenseRefund expenseRefund) throws CreateException {
        expenseRefundFacade.remove(expenseRefund);
    }

    public List<ExpenseRefund> findAll() {
        return expenseRefundFacade.findAll();
    }

    public ExpenseRefund findByName(String name) {
        String sql = "SELECT a FROM ExpenseRefund a WHERE a.name=:name";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", name);
        return expenseRefundFacade.findbyQuerySingle(sql, params);
    }
    
    /*
    *SECTION NEW ###############################################################
    */

     public List<ExpenseRefund> findByExpensePayment(ExpensePayment expensePayment) {
        String sql = "SELECT a FROM ExpenseRefund a WHERE a.expensePayment=:expensePayment";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("expensePayment", expensePayment);
        return expenseRefundFacade.findbyQuery(sql, params);
    }
  
}
